(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

let version = "1.0-svn"
let gentype = ref "CBoth"
let lusi_filename = ref ""

let options =
  [ "-aer_wcc", Arg.Unit (fun () -> Options.aer_wcc := true; Options.aer_format := true; Options.bool_is_stdbool := false), "produce AER code for wcc";
    "-bool_is_stdbool", Arg.Set Options.bool_is_stdbool, "map Prelude bool type to C bool from <stdbool.h> (WILL BE SUPPRESSED-THIS IS THE DEFAULT NOW))";
    "-bool_is_int", Arg.Clear Options.bool_is_stdbool, "map Prelude bool type to int (default is to use bool from C99)";
    "-d", Arg.Set_string Options.dest_dir, "produces code in the specified directory";
    "-gentype", Arg.Set_string gentype, "ask for the generation of imported C files (allowed values: CHeader, CBody, CBoth, CLustreBody, CLustreBoth)";
    "-lusi", Arg.Set_string lusi_filename, "the Lustre interface (a.k.a. .lusi) file";
    "-node", Arg.Set_string Options.main_node, "specifies the main node";
    "-no_io_code", Arg.Clear Options.io_code, "produce no code for sensors/actuators";    
    "-real_is_double", Arg.Set Options.real_is_double, "map Prelude real type to double (WILL BE SUPPRESSED-THIS IS THE DEFAULT NOW)";
    "-real_is_float", Arg.Clear Options.real_is_double, "map Prelude real type to float (default is double)";
    "-version", Arg.Unit (fun () -> print_endline version), " Display the version";]

type gen_IN_type =
  No
| CHeader
| CBody
| CBoth
| CLustreBody
| CLustreBoth

let gen_IN_kind () =
  match !gentype with
  | "cheader" | "CHeader" | "header" | "Header" | "h" | "H" -> CHeader
  | "cbody" | "CBody" -> CBody
  | "cboth" | "CBoth" | "cb" | "CB" -> CBoth
  | "clustreboth" | "CLustreBoth" | "clustre" | "CLustre" | "CLB" | "clb" -> CLustreBoth
  | "clustrebody" | "CLustreBody" -> CLustreBody
  | _ -> failwith ("Unknown generate import node C file directive " ^ !gentype)

let gen_IN_header () =
  gen_IN_kind () = CHeader
  || gen_IN_kind () = CBoth
  || gen_IN_kind () = CLustreBoth

let gen_IN_body () =
  gen_IN_kind () = CBody
  || gen_IN_kind () = CLustreBody
  || gen_IN_kind () = CBoth
  || gen_IN_kind () = CLustreBoth

let gen_IN_forLustrec () =
  gen_IN_kind () = CLustreBody
  || gen_IN_kind () = CLustreBoth
