(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** A type implementing an associative array using association lists
 * The data structure is organized as two lists.
 * The first one contains all values inside our environment (the 'past'),
 * while the second contains future values to include (the 'future').
 * The important feature is that we store all operations in order,
 * thus allowing us to deterministcally rollback/skip to a specific point.
 * Currently, all operations run in O(N).
 * One possibility would be to actually use a map for all access
 * and to keep a ledger containing all operations.
 *)

let from_list l = [], l

let to_list (e,l) = List.rev_append e l

let append_list (e,l) l' = e, l@l'

let next (_,l) = let _,v = List.hd l in v

let step (e,l) =
  match l with
  | [] -> e,l
  | x::xs -> x::e, xs

let exists (e,_) k = List.mem_assoc k e
let get (e,_) k = List.assoc k e

let rec rollback_until (e,l) k =
  match e with
  | (a,b)::xs when a=k -> xs, (a,b)::l
  | x::xs -> rollback_until (xs,x::l) k
  | [] -> raise Not_found

let rec skip_until (e,l) k =
  match l with
  | (a,b)::xs when a=k -> e,l
  | x::xs -> skip_until (step (e,l)) k
  | [] -> raise Not_found

let filter p (e, l) =
  List.filter p e, List.filter p l

let filter_list p (e,l) =
  List.filter p (List.rev_append e l)

let map_list f (e,l) =
  List.map f (List.rev_append e l)
