(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Independent task set structure. *)

open Corelang
open Task_graph

(* For the debug of the waters challenge *)
(* let task_set_bound = Some 10*)
let task_set_bound = None

type error =
    No_sensor_wcet of ident
  | No_actuator_wcet of ident
  | Not_sensor of ident
  | Not_actuator of ident

exception Error of Location.t * error

(** Communication protocol, on the producer side *)
type write_proto = {wbuf_size: int;
                    write_pref: bool array;
                    write_pat: bool array;
                    wbuf_init: atom option}

(** Communication protocol, on the consumer side *)
type read_proto = {rbuf_size: int;
                   change_pref: bool array;
                   change_pat: bool array;
                   rbuf_init: atom option}

type vref =
  | ConstVal of ident
  | MainVar of ident (** Input/output of the main node *)
  | TaskVar of ident*(ident*tag) (** Input/output of a node instance *)

type task_kind = Sensor | Actuator | CstTask | StdTask | MergeTask | WCETTask of ident*int

type cond =
    {cond_var: var_decl;
     mutable cond_pred: vref*read_proto;
     cond_val: bool}

type task =
    {task_id: ident*tag;
     task_kind: task_kind;
     (** Input variables, with pred vertex and read protocol *)
     mutable task_inputs: (var_decl*vref*read_proto) list;
     (** Output variables with the list of their succ vertices *)
     mutable task_outputs: (var_decl*(vref*write_proto) list) list;
     (** Activation conditions due to When *)
     mutable task_conds: cond list;
     task_wcet: int;
     mutable task_deadline: Deadlines.deadline_word;
     task_period: int;
     task_release: int}

type task_set = (ident*tag, task) Hashtbl.t

let dummy_vref = MainVar ""

(** {5 Various utility functions } *)

let task_kind ndesc =
  match ndesc.ndvar_nd_kind with
  | RegularNode -> StdTask
  | MergeNode -> MergeTask
  | WCETNode (id,n) -> WCETTask (id,n)

(** Assign type & clock of vdecl to those of v' *)
let type_predef_var vdecl v' =
  {vdecl with var_type=v'.var_type; var_clock=v'.var_clock}

let vref_of_vertex v =
  match v.vertex_desc with
  | Constant (cst,_) -> ConstVal cst
  | Var v -> MainVar v.var_id
  | NodeVar ndesc -> TaskVar (ndesc.ndvar_var_decl.var_id,(ndesc.ndvar_node_desc.nodei_id,ndesc.ndvar_tag))
  | _ -> failwith "Internal error"

let abs_release t n =
  t.task_release + (n*t.task_period)

let dummy_write_pattern = {wbuf_size = 0;
                           write_pref = Array.make 0 false;
                           write_pat = Array.make 0 false;
                           wbuf_init = None}

let dummy_read_pattern = {rbuf_size = 0;
                          change_pref = Array.make 0 false;
                          change_pat = Array.make 0 false;
                          rbuf_init = None}

let taskid_of_vertex v =
  match v.vertex_desc with
  | Constant (cst,tag) -> (cst, tag)
  | Var v ->
      (v.var_id,0)
  | NodeVar ndesc ->
      ndesc.ndvar_node_desc.nodei_id,ndesc.ndvar_tag
  | PredefOpVar _ -> failwith "Internal error"

let varid_of_vref v =
  match v with
  | ConstVal cst -> failwith "Internal error taskid_of_vref"
  | MainVar io -> io
  | TaskVar (v,_) -> v

let taskid_of_vref v =
  match v with
  | ConstVal cst -> failwith "Internal error taskid_of_vref"
  | MainVar io -> (io,0)
  | TaskVar (_,tid) -> tid

let task_of_vertex task_set v =
  Hashtbl.find task_set (taskid_of_vertex v)

let input_vids_of_task t =
  match t.task_kind with
  | Sensor | CstTask -> []
  | Actuator ->
      let (vid,_)=t.task_id in
      [vid]
  | StdTask | MergeTask | WCETTask _ ->
      let (nid,ntag) = t.task_id in
      let ins =
        List.map
          (fun (v,_,_) -> nid^(string_of_int ntag)^"_"^v.var_id)
          t.task_inputs in
      let conds =
        List.map
          (fun c -> nid^(string_of_int ntag)^"_"^c.cond_var.var_id)
          t.task_conds in
      conds@ins

let uses_read_protocol proto =
  (Array.length proto.change_pref > 1) ||
  (Array.length proto.change_pat > 1)

let uses_write_protocol proto =
  (Array.length proto.write_pref > 1) ||
  (Array.length proto.write_pat > 1)

let any_uses_read_proto inputs = List.exists (fun (_,_,x) -> uses_read_protocol x) inputs

let any_uses_write_proto outputs trace_instances =
  List.exists
    (fun (_,x) -> uses_write_protocol x)
    (List.concat (List.map (fun (_,x) -> x) outputs)) ||
    (trace_instances && Options.trace_instances ())

let array_collapse_threshold = 4

let would_collapse array =
  let (last_v, c, res) =
    Array.fold_left
      (fun (prec_v, cpt, res) v ->
        match res,prec_v with
        | true,_ -> prec_v,cpt,true
        | _,None -> Some v, 1, res
        | _,Some pv ->
          if pv = v then
              Some v, cpt+1, res
          else
              Some v, 1, res || cpt >= array_collapse_threshold)
      (None,0,false) array
  in
  match last_v with
  | None -> res
  | Some lv -> res || (c >= array_collapse_threshold)

let needs_idxs task =
  let needs_idxr = any_uses_read_proto task.task_inputs in
  let needs_idxw = any_uses_write_proto task.task_outputs false in
  let needs_idx = needs_idxr || needs_idxw in
  let protos =
    List.map
      (fun (_,_,p) -> [p.change_pat;p.change_pref])
      task.task_inputs
    @
    List.map
      (fun (_,p) -> [p.write_pat;p.write_pref])
      (List.concat (List.map (fun (_,x) -> x) task.task_outputs))
    |>
    List.concat
  in
  let needs_sidx = List.exists would_collapse protos in
  needs_idx, needs_sidx

(* Complexity is quite poor (due to searching for read_proto) *)
let coms_list task_set =
  let read_proto tid vid =
    let t = Hashtbl.find task_set tid in
    try
      let _,_,read_proto =
        List.find (fun (vdecl,_,_) -> vdecl.var_id = vid) t.task_inputs in
      read_proto
    with Not_found ->
      let _,read_proto = (List.find
                            (fun cond -> cond.cond_var.var_id = vid)
                            t.task_conds).cond_pred in
      read_proto
  in
  Hashtbl.fold
    (fun tpred_id tpred coms ->
      List.fold_left
        (fun more_coms (vpred,succs) ->
          List.fold_left
            (fun even_more_coms (succ_ref,write_proto) ->
              match succ_ref with
              | ConstVal _ -> even_more_coms
              | _ ->
                  let tsucc_id = taskid_of_vref succ_ref in
                  let vsucc_id = varid_of_vref succ_ref in
                  let read_proto = read_proto tsucc_id vsucc_id in
                  let com = tpred_id,vpred.var_id, write_proto,tsucc_id,vsucc_id,read_proto in
                  com::even_more_coms)
            more_coms succs)
        coms tpred.task_outputs)
    task_set []

let filter_io task_set =
  if !Options.io_code then
      task_set
  else
    begin
      Utils.filter_hashtbl (* To be replaced by Hashtbl version in OCaml 4.03 *)
        (fun _ t -> match t.task_kind with
        | Sensor | Actuator -> None
        | _ -> Some t)
        task_set
    end

(** {4 Pretty printing } *)

open Format

let pp_task_id (nid,tag) =
  if !Options.print_tasks then
    begin
      print_string nid
    end
  else
    begin
      print_string nid;
      print_string "(";
      print_int tag;
      print_string ")"
    end

let pp_task_conds conds =
  Utils.pp_list conds
    (fun cond -> if not cond.cond_val then print_string "!"; print_string cond.cond_var.var_id)
    "" "" "&"

let pp_task t =
  if !Options.print_tasks then
    begin
      pp_task_id t.task_id;
      print_string " ";
      print_int t.task_period;
      print_string " ";
      print_int t.task_wcet;
      print_string " ";
      print_int t.task_release;
      print_string " ";
      Deadlines.print_dw t.task_deadline;
      print_string " ";
      pp_task_conds t.task_conds
    end
  else
    begin
      pp_task_id t.task_id;
      print_string ": ";
      print_string "T: ";
      print_int t.task_period;
      print_string ", C: ";
      print_int t.task_wcet;
      print_string ", r: ";
      print_int t.task_release;
      print_string ", d: ";
      Deadlines.print_dw t.task_deadline
    end

let pp_read_proto proto =
  print_string ", read protocol: buffer size is ";
  print_int proto.rbuf_size;
  Utils.pp_array proto.change_pref print_bool ", pattern: " "." ".";
  Utils.pp_array proto.change_pat print_bool "(" ")" "."

let pp_write_proto proto =
  print_string ", write protocol: buffer size is ";
  print_int proto.wbuf_size;
  Utils.pp_array proto.write_pref print_bool ", pattern: " "." ".";
  Utils.pp_array proto.write_pat print_bool "(" ")" "."

let pp_vref vref =
  match vref with
  | ConstVal cst -> print_string cst
  | MainVar vid ->
      print_string vid
  | TaskVar (vid,(nid,tag)) ->
      pp_inst_id (nid,tag);
      print_string "_";
      print_string vid

let pp_vref_simple vref =
match vref with
  | ConstVal cst -> print_string cst
  | MainVar vid ->
      print_string vid
  | TaskVar (vid,(nid,tag)) ->
      print_string nid

let pp_task_precs s t =
  List.iter
    (fun (v,prec,proto) ->
      let tpred = Hashtbl.find s (taskid_of_vref prec) in
      if (tpred.task_period = t.task_period) then
        begin
          pp_vref_simple prec;
          print_string "->";
          pp_task_id t.task_id;
          print_newline ()
         end)
    t.task_inputs

let pp_task_protos t =
  List.iter
    (fun (v,_,proto) ->
      pp_inst_id t.task_id;
      print_string "_";
      print_string v.var_id;
      pp_read_proto proto;
      print_newline ())
    t.task_inputs;
  List.iter
    (fun (v,protos) ->
      Utils.pp_list protos
        (fun (succ,proto) ->
          pp_inst_id t.task_id;
          print_string "_";
          print_string v.var_id;
          print_string " to ";
          pp_vref succ;
          pp_write_proto proto;
          print_newline ())
        "" "" "\n")
    t.task_outputs

let pp_task_set_protos s =
  Hashtbl.iter (fun _ t -> if t.task_kind <> CstTask then pp_task_protos t) s

let pp_task_set s =
  Hashtbl.iter (fun _ t -> if t.task_kind <> CstTask then begin
    pp_task t;print_newline () end ) s

let pp_task_set_simple_precs s =
  Hashtbl.iter (fun _ t -> if t.task_kind <> CstTask then begin
    pp_task t;print_newline () end ) s;
  Hashtbl.iter (fun _ t -> if t.task_kind <> CstTask then begin
    pp_task_precs s t end ) s

let pp_error = function
  | No_sensor_wcet id ->
      print_string ("No wcet provided for sensor "^id);
      print_newline ()
  | No_actuator_wcet id ->
      print_string ("No wcet provided for actuator "^id);
      print_newline ()
  | Not_sensor id ->
      print_string ("Main node parameter "^id^" was not declared as a sensor");
      print_newline ()
  | Not_actuator id ->
      print_string ("Main node parameter "^id^" was not declared as a sensor");
      print_newline ()
