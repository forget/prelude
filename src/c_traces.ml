(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate C code related to execution tracing. *)

open C_utils
open Corelang
open Task_set
open Format  

let trace_buf_maxsize = "1024"

let pp_trace_buf_alloc out_f task =
  if Options.trace_lttng () then
    begin
      match task.task_kind with
      | CstTask -> ()
      | StdTask | MergeTask | WCETTask _ ->
          fprintf out_f "static char %s [%s];@ " trace_ins_buf_name trace_buf_maxsize;
          fprintf out_f "static char %s [%s];@ @ " trace_outs_buf_name trace_buf_maxsize
      | Sensor ->
          fprintf out_f "static char %s [%s];@ @ " trace_outs_buf_name trace_buf_maxsize
      | Actuator ->
          fprintf out_f "static char %s [%s];@ @ " trace_ins_buf_name trace_buf_maxsize
    end
    
let pp_trace_inputs_pat_printer out_f task (decl, ref, proto) =
  let valfmt = fmtstring_of_type decl.var_type in
  if Options.trace_instances () then
    match ref with
    | ConstVal cst ->
        fprintf out_f "%s=%s" decl.var_id cst
    | _ ->
        fprintf out_f "%s=<%s,%s@@%%d>" decl.var_id valfmt (extname_of_vref ref)
  else
    fprintf out_f "%s=%s" decl.var_id valfmt

let pp_trace_inputs_pat out_f task =
  Print.list_printer_from_printer "\"@,\"" " \"@,\"" "\"@,\""
    (fun out_f t -> (pp_trace_inputs_pat_printer out_f task t))
    out_f task.task_inputs

let pp_trace_output_val_pat out_f task =
  let vout,_ = List.hd task.task_outputs in
  let valfmt = fmtstring_of_type vout.var_type in
  if Options.trace_printf () then
    fprintf out_f "=> %s=%s" vout.var_id valfmt
  else
    fprintf out_f "%s=%s" vout.var_id valfmt

let pp_trace_outputs_struct_pat_printer out_f task (decl, list) =
  let valfmt = fmtstring_of_type decl.var_type in
  fprintf out_f "%s=%s" decl.var_id valfmt

let pp_trace_output_struct_pat out_f task =
  if Options.trace_printf () then
    fprintf out_f "=> ";
  Print.list_printer_from_printer "\"@,\"" " \"@,\"" "\"@,\""
    (fun out_f t -> (pp_trace_outputs_struct_pat_printer out_f task t))
    out_f task.task_outputs

let pp_trace_input_name out_f task vin vref_pred proto =
      let vref_in = vref_of_task_var task vin in
      let buf_name = combuffer_name vref_pred vref_in in
      fprintf out_f "%s" buf_name;
      if proto.rbuf_size > 1 then
        fprintf out_f "[%s]" (read_cell_pointer_name vin);
      if Options.trace_instances () then
        begin
          fprintf out_f ".value, ";
          fprintf out_f "%s" buf_name;
          if proto.rbuf_size > 1 then
            fprintf out_f "[%s]" (read_cell_pointer_name vin);
          fprintf out_f ".instance"
        end

let pp_trace_inputs_val_printer out_f task (decl, ref, proto) =
  match ref with ConstVal _ -> ()
  | _ ->
      fprintf out_f ",@,";
      pp_trace_input_name out_f task decl ref proto

let pp_trace_inputs_val out_f task =
  List.iter (pp_trace_inputs_val_printer out_f task) task.task_inputs

let pp_trace_output_val_val out_f task =
  let vout,_ = List.hd task.task_outputs in
  fprintf out_f ",@,%s" vout.var_id

let pp_trace_outputs_struct_val_printer out_f task (decl, list) =
  fprintf out_f ",@,%s.%s " (outstruct_name task.task_id) decl.var_id
let pp_trace_output_struct_val out_f task =
  List.iter (pp_trace_outputs_struct_val_printer out_f task) task.task_outputs

let pp_trace_node out_f task =
  if Options.trace_printf () then
    begin
      fprintf out_f "fflush(NULL);@ fprintf(stderr, \"";
      begin
        match task.task_kind with
        | StdTask | MergeTask | WCETTask _ ->
            if Options.trace_instances () then
              fprintf out_f "NODE %s@@%%d " (external_name task.task_id)
            else
              fprintf out_f "NODE %s " (external_name task.task_id);
            pp_trace_inputs_pat out_f task;
            fprintf out_f " ";
            if List.length task.task_outputs = 1 then
              pp_trace_output_val_pat out_f task
            else
              pp_trace_output_struct_pat out_f task;
            fprintf out_f "\\n\"";
            if Options.trace_instances () then
              fprintf out_f ",@,%s" inst_cpt_name;
            pp_trace_inputs_val out_f task;
            if List.length task.task_outputs = 1 then
              pp_trace_output_val_val out_f task
            else
              pp_trace_output_struct_val out_f task
        | Sensor ->
            if Options.trace_instances () then
              fprintf out_f "SENS %s@@%%d " (external_name task.task_id)
            else
              fprintf out_f "SENS %s " (external_name task.task_id);
            pp_trace_output_val_pat out_f task;
            fprintf out_f "\\n\"";
            if Options.trace_instances () then
              fprintf out_f ",@,%s" inst_cpt_name;
            pp_trace_output_val_val out_f task
        | Actuator ->
            if Options.trace_instances () then
              fprintf out_f "ACTU %s@@%%d " (external_name task.task_id)
            else
              fprintf out_f "ACTU %s " (external_name task.task_id);
            pp_trace_inputs_pat out_f task;
            fprintf out_f "\\n\"";
            if Options.trace_instances () then
              fprintf out_f ",@,%s" inst_cpt_name;
            pp_trace_inputs_val out_f task
        | CstTask -> () (* Printed directly when printing input of consuming node *)
      end;
      fprintf out_f ");@ "
    end
  else if Options.trace_lttng () then
    begin
      match task.task_kind with
        | StdTask | MergeTask | WCETTask _ ->
            fprintf out_f "snprintf(%s, %s, \"" trace_ins_buf_name trace_buf_maxsize;
            pp_trace_inputs_pat out_f task;
            fprintf out_f "\"";
            pp_trace_inputs_val out_f task;
            fprintf out_f ");@ ";
            fprintf out_f "snprintf(%s, %s, \"" trace_outs_buf_name trace_buf_maxsize;
            if List.length task.task_outputs = 1 then
              begin
                pp_trace_output_val_pat out_f task;
                fprintf out_f "\"";
                pp_trace_output_val_val out_f task
              end
            else
              begin
                pp_trace_output_struct_pat out_f task;
                fprintf out_f "\"";
                pp_trace_output_struct_val out_f task
              end;
            fprintf out_f ");@ ";
            fprintf out_f "tracepoint(prelude, task, \"%s\", %s, %s, %s);@ "
              (external_name task.task_id) inst_cpt_name trace_ins_buf_name trace_outs_buf_name
        | Sensor ->
            fprintf out_f "snprintf(%s, %s, \"" trace_outs_buf_name trace_buf_maxsize;
            if List.length task.task_outputs = 1 then
              begin
                pp_trace_output_val_pat out_f task;
                fprintf out_f "\"";
                pp_trace_output_val_val out_f task
              end
            else
              begin
                pp_trace_output_struct_pat out_f task;
                fprintf out_f "\"";
                pp_trace_output_struct_val out_f task
              end;
            fprintf out_f ");@ ";
            fprintf out_f "tracepoint(prelude, sensor, \"%s\", %s, %s);@ "
              (external_name task.task_id) inst_cpt_name trace_outs_buf_name
        | Actuator ->
            fprintf out_f "snprintf(%s, %s, \"" trace_ins_buf_name trace_buf_maxsize;
            pp_trace_inputs_pat out_f task;
            fprintf out_f "\"";
            pp_trace_inputs_val out_f task;
            fprintf out_f ");@ ";
            fprintf out_f "tracepoint(prelude, actuator, \"%s\", %s, %s);@ "
              (external_name task.task_id) inst_cpt_name trace_ins_buf_name
        | CstTask -> () (* Printed directly when printing input of consuming node *)
    end
    
