(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Names for many things in the generated C code *)

open Types
open Corelang
open Task_graph
open Task_set

let vref_of_task_var task v =
  match task.task_kind with
  | CstTask -> failwith "Internal error: vref_of_task_var"
  | Sensor | Actuator -> MainVar v.var_id
  | StdTask | MergeTask | WCETTask _ -> TaskVar (v.var_id, task.task_id)

let task_struct_name () =
  if !Options.no_encoding
  then
    begin
      if !Options.aer_format then
        "nonencoded_aer_task_params"
      else
        "nonencoded_task_params"
    end
  else "encoded_task_params"

let prec_struct_name = "multirate_precedence"
    
let job_prec_struct_name = "job_prec"

let buffer_struct_name = "buffer_desc"
    
let com_struct_name = "com_t"

let read_proto_struct_name = "read_proto_t"

let write_proto_struct_name = "write_proto_t"
    
let task_name (nid,ntag) =
  nid^(string_of_int ntag)

let thread_name (nid,ntag) =
  "t"^nid^(string_of_int ntag)

let fun_name_init (nid,ntag) =
  nid^"_"^(string_of_int ntag)^"_I"
  
let fun_name (nid,ntag) =
  nid^"_"^(string_of_int ntag)^"_fun"

let fun_name_A (nid,ntag) =
  nid^"_"^(string_of_int ntag)^"_A"
                              
let fun_name_E (nid,ntag) =
  nid^"_"^(string_of_int ntag)^"_E"

let fun_name_R (nid,ntag) =
  nid^"_"^(string_of_int ntag)^"_R"
                              
let external_name (nid,ntag) =
  nid

let wcetlt_name nid =
  "is_wcet_lt_"^nid

let actuator_name tid =
  let (nid,_) = tid in
  "output_"^nid

let sensor_name tid =
  let (nid,_) = tid in
  "input_"^nid

let outstruct_name (nid,ntag) =
  (fun_name (nid,ntag))^"_lwrite"

let outstruct_type (nid,ntag) =
  "struct "^nid^"_lwrite_t"

let load_name (nid,ntag) =
  "load_"^nid^(string_of_int ntag)

let inst_cpt_name = "instance"

let trace_ins_buf_name = "trace_ins_buf"
let trace_outs_buf_name = "trace_outs_buf"

let extname_of_vref v =
  match v with
  | MainVar vid -> vid
  | TaskVar (vid,(nid,ntag)) -> nid
  | ConstVal _ -> failwith "Internal error: vname_of_vref"

let vname_of_vref v =
  match v with
  | MainVar vid -> vid
  | TaskVar (vid,(nid,ntag)) ->
      nid^(string_of_int ntag)^"_"^vid
  | ConstVal _ -> failwith "Internal error: vname_of_vref"

(* Warning: does not print array dimension here *)
let rec tystring_of_type ty =
  match (repr ty).tdesc with
  | Tint -> "int"
  | Tchar -> "char"
  | Tfloat -> if !Options.real_is_double then "double" else "float"
  | Tbool  -> if !Options.bool_is_stdbool then "bool" else "int"
  | Tarray (ty',_) -> tystring_of_type ty' (* We cannot output the dimension here. That's C... *)
  | _ -> print_ty ty; print_newline (); failwith "Internal error tystring_of_type"

(** Returns the string corresponding to the dimensions of ty (for array types) *)
let rec dim_string ty =
  match (repr ty).tdesc with
  | Tarray (ty',n) ->
      (dim_string ty')^"["^(string_of_int n)^"]"
  | _ -> ""

let string_of_type_var ty vname =
  let ty_str = tystring_of_type ty in
  let dim_str = dim_string ty in
  ty_str^" "^vname^dim_str

let cstring_of_bool b =
  if b then
     if !Options.bool_is_stdbool then
       "true"
     else
       "1"
  else
    if !Options.bool_is_stdbool then
      "false"
    else
      "0"
                      
let fmtstring_of_type ty =
  match (repr ty).tdesc with
  | Tint -> "%d"
  | Tchar -> "%c"
  | Tfloat -> if !Options.real_is_double then "%f" else "%f"
  | Tbool  -> if !Options.bool_is_stdbool then "%d" else "%d"
  | Tarray _ -> failwith "fmtstring_of_type: array unsupported"
  | _ -> failwith "Internal error fmtstring_of_type"

let combuffer_name from_v to_v =
  (vname_of_vref from_v)^"_"^(vname_of_vref to_v)

let local_rbuffer_name (nid,ntag) vid =
  (fun_name (nid,ntag))^"_"^vid^"_locread"

let local_wbuffer_name (nid,ntag) vid =
  (fun_name (nid,ntag))^"_"^vid^"_locwrite"
                                  
let combuffer_id buf_name =
  buf_name^"_id"

let combuffer_init from_v to_v =
  (combuffer_name from_v to_v)^"_init"
              
let outvar_name task vout =
  if List.length task.task_outputs > 1 then
    (outstruct_name task.task_id)^"."^vout.var_id
  else
    local_wbuffer_name task.task_id vout.var_id

let read_proto_name tid vid =
  (task_name tid)^"_"^vid^"_change"

let read_cell_pointer_name vin =
  vin.var_id^"_rcell"

let write_proto_name tpred_id vpred_id vref_succ =
  (task_name tpred_id)^"_"^vpred_id^"_"^(vname_of_vref vref_succ)^"_write"

let write_proto_name_bis tpred_id vpred_id (tsucc_name,tsucc_tag) vsucc_id =
  if tsucc_name=vsucc_id then
    (task_name tpred_id)^"_"^vpred_id^"_"^vsucc_id^"_write"
  else
    (task_name tpred_id)^"_"^vpred_id^"_"^(task_name (tsucc_name,tsucc_tag))^"_"^vsucc_id^"_write"
                                                                               
let write_cell_pointer_name vout vref_succ =
    vout.var_id^"_"^(vname_of_vref vref_succ)^"_wcell"

let write_proto_pref_name = "write_pref"
    
let write_proto_pref_size_name = "wpref_size"
    
let write_proto_pat_name = "write_pat"
    
let write_proto_pat_size_name = "wpat_size"

let read_proto_pref_name = "change_pref"
let read_proto_pref_size_name = "rpref_size"
let read_proto_pat_name = "change_pat"
let read_proto_pat_size_name = "rpat_size"

let proto_array_name proto_name pref =
  proto_name^(if pref then "_pref" else "_pat")

let nb_tasks_name = "PLUD_TASK_NUMBER"

let static_task_set_name = "static_task_set"

let dword_pref_name tname = tname^"_dwpref"

let dword_pat_name tname = tname^"_dwpat"

let nb_precs_name = "PLUD_PREC_NUMBER"

let static_prec_set_name = "static_prec_set"

let nb_coms_name = "PLUD_COM_NUMBER"

let static_com_set_name = "static_com_set"

let nb_buffers_name = "PLUD_BUFFER_NUMBER"
    
let static_buffer_set_name = "static_buffer_set"
    
let task_name_of_vertex src =
  task_name (taskid_of_vertex src)

let prec_pref_name (src,dst) =
  (task_name_of_vertex src)^"_"^
  (task_name_of_vertex dst)^"_pcpref"

let prec_pat_name (srcid,dstid) =
  (task_name_of_vertex  srcid)^"_"^
  (task_name_of_vertex dstid)^"_pcpat"

let types_filename nodename =
  nodename                                 
                                 
let includes_filename nodename =
  nodename^"_includes"

let extbuffers_filename nodename =
  nodename^"_extbuffers"

let include_sens_act_filename nodename =
  (includes_filename nodename)^"_sensors_actuators"
              
let write_global_var_name tpred_id vpred_id vref_succ =
  (task_name tpred_id)^"_"^vpred_id^"_"^(vname_of_vref vref_succ)

let init_idx = "_idx_i"
let init_sidx = "_idx_j"
                                           
let create_and_get_gendir basename =
  let dir =
    if !Options.dest_dir = "" then
      basename^"_c"
    else
      !Options.dest_dir in
  ignore(Utils.make_dir (dir));
  dir
    
let zero_atom_string a =
  match a with
  | AConst_int _ | AConst_float _ -> "0"
  | AConst_char _ -> "\'0\'"
  | AConst_bool _ -> cstring_of_bool false
  | AIdent _ -> failwith "Internal error: requested zero_string of AIdent"

(* Returns the number of successive 0 at the end of array *)    
let zero_tail_size zero_test array =
  let l = List.rev (Array.to_list array) in
  let rec aux sub =
    match sub with
      [] -> 0
    | x::rest -> if (zero_test x) then 1 + (aux rest) else 0
  in
  aux l
    
let pp_bool_include out_f =
  if !Options.bool_is_stdbool then
    Format.fprintf out_f "#include <stdbool.h>@,"
  else if !Options.aer_wcc then
    Format.fprintf out_f "#include \"no-std-include.h\"@,"

    
(** Prints array litteral (initialization) with values described by
   [array]. If the array contains a tail of zeros, replaces the tail by
   a single 0. Warning, for this to work, the array being initialized
   must be declared with the correct size. Initialization of unsized arrays
   will cause bugs. *)
let print_array_litteral out_f printer zero_string zero_test array =
  let tail_size = zero_tail_size zero_test array in
  if  tail_size = 0 then
    Print.array_printer_from_printer "{" ", " "}"
      (fun out_f (_,v) -> printer out_f v) out_f array
  else
    begin
      Format.fprintf out_f "{";
      for i=0 to (Array.length array) - 1 - tail_size do
        printer out_f array.(i);
        Format.fprintf out_f ", "
      done;
      Format.fprintf out_f "%s}" zero_string
    end

      
