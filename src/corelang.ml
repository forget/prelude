(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)
open Format

type ident = Utils.ident
type rat = Utils.rat
type tag = Utils.tag

type atom =
  | AConst_int of int
  | AConst_char of char
  | AConst_float of float
  | AConst_bool of bool
  | AIdent of ident

type constant =
  | Const_int of int
  | Const_char of char
  | Const_float of float
  | Const_bool of bool

type type_dec =
    {ty_dec_desc: type_dec_desc;
     ty_dec_loc: Location.t}

and type_dec_desc =
  | Tydec_any
  | Tydec_int
  | Tydec_char
  | Tydec_float
  | Tydec_bool
  | Tydec_array of type_dec * int

type deadline_dec = int option

type clock_dec =
    {ck_dec_desc: clock_dec_desc;
     ck_dec_loc: Location.t}

and clock_dec_desc =
  | Ckdec_any
  | Ckdec_pclock of int * rat

type var_decl = 
    {var_id: ident;
     var_dec_type: type_dec;
     var_dec_clock: clock_dec;
     var_dec_deadline: deadline_dec;
     mutable var_type: Types.type_expr;
     mutable var_clock: Clocks.clock_expr;
     var_loc: Location.t;
     mutable var_order: int}

type expr =
    {expr_tag: tag;
     expr_desc: expr_desc;
     mutable expr_type: Types.type_expr;
     mutable expr_clock: Clocks.clock_expr;
     expr_loc: Location.t}

and expr_desc =
  | Expr_atom of atom
  | Expr_const of constant
  | Expr_ident of ident
  | Expr_tuple of expr list
  | Expr_fby of atom * expr
  | Expr_concat of atom * expr
  | Expr_tail of expr
  | Expr_when of expr * ident
  | Expr_whennot of expr * ident
  | Expr_merge of ident * expr * expr
  | Expr_appl of ident * expr
  | Expr_uclock of expr * int
  | Expr_dclock of expr * int
  | Expr_phclock of expr * rat
  | Expr_rate of expr * clock_dec
  | Expr_wcetlt of ident * expr * int
  | Expr_map of ident * int * expr

type eq =
    {eq_lhs: ident list;
     eq_rhs: expr;
     eq_loc: Location.t}

type node_desc =
    {node_id: ident;
     mutable node_type: Types.type_expr;
     mutable node_clock: Clocks.clock_expr;
     node_inputs: var_decl list;
     node_outputs: var_decl list;
     node_locals: var_decl list;
     node_eqs: eq list}

type imported_node_desc =
    {nodei_id: ident;
     mutable nodei_type: Types.type_expr;
     mutable nodei_clock: Clocks.clock_expr;
     nodei_inputs: var_decl list;
     nodei_outputs: var_decl list;
     nodei_wcet: int}

type sensor_desc =
    {sensor_id: ident;
     sensor_wcet: int}

type actuator_desc =
    {actuator_id: ident;
     actuator_wcet: int}

type const_desc =
    {const_id: ident;
     const_value: constant}

type top_decl_desc =
  | Node of node_desc
  | ImportedNode of imported_node_desc
  | SensorDecl of sensor_desc
  | ActuatorDecl of actuator_desc
  | ConstDecl of const_desc

type top_decl =
    {top_decl_desc: top_decl_desc;
     top_decl_loc: Location.t}

type program = top_decl list

type assoc_program = (ident * top_decl) list * (ident * top_decl) list

type error =
    Main_not_found
  | Main_wrong_kind
  | No_main_specified

exception Error of error
exception Unbound_type of ident*Location.t

let dummy_type_dec = {ty_dec_desc=Tydec_any; ty_dec_loc=Location.dummy}
let dummy_clock_dec = {ck_dec_desc=Ckdec_any; ck_dec_loc=Location.dummy}

let predef_merge =
  (* Note that clocks and types are polymorphic, they will be
     instantiated late, at task graph creation *)
  let ty,ck = (Types.new_var (), Clocks.new_var false) in
  let cond = {var_id="c";
              var_dec_type=dummy_type_dec;
              var_dec_clock=dummy_clock_dec;
              var_dec_deadline=None;
              var_type=Type_predef.type_bool;
              var_clock=ck;
              var_loc=Location.dummy;
              var_order=1}
  in
  let i1 = {var_id="i1";
            var_dec_type=dummy_type_dec;
            var_dec_clock=dummy_clock_dec;
            var_dec_deadline=None;
            var_type=ty;
            var_clock=ck;
            var_loc=Location.dummy;
            var_order=2}
  in
  let i2 = {var_id="i2";
            var_dec_type=dummy_type_dec;
            var_dec_clock=dummy_clock_dec;
            var_dec_deadline=None;
            var_type=ty;
            var_clock=ck;
            var_loc=Location.dummy;
            var_order=3}
  in
  let o = {var_id="o";
           var_dec_type=dummy_type_dec;
           var_dec_clock=dummy_clock_dec;
           var_dec_deadline=None;
           var_type=ty;
           var_clock=ck;
           var_loc=Location.dummy;
           var_order=1}
  in      
  {top_decl_desc=ImportedNode
     {nodei_id="merge";
      nodei_type=Type_predef.type_bin_poly_op;
      nodei_clock=Clock_predef.ck_bin_univ;
      nodei_inputs = [cond;i1;i2];
      nodei_outputs = [o];
      nodei_wcet = 1; (* TODO: make this declarable *)};
   top_decl_loc=Location.dummy}

let predef_wcetlt =
  (* Inputs are empty, they will be known at task graph creation *)
  let o = {var_id="o";
           var_dec_type=dummy_type_dec;
           var_dec_clock=dummy_clock_dec;
           var_dec_deadline=None;
           var_type=Type_predef.type_int;
           var_clock=Clocks.new_var false;
           var_loc=Location.dummy;
           var_order=1}
  in      
  {top_decl_desc=ImportedNode
     {nodei_id="wcetlt";
      nodei_type=Type_predef.type_univ_int;
      nodei_clock=Clock_predef.ck_bin_univ;
      nodei_inputs = [];
      nodei_outputs = [o];
      nodei_wcet = 1; (* TODO: make this declarable *)};
   top_decl_loc=Location.dummy}

let prog_init =
  let l = Iterenv.from_list ["merge", predef_merge; "wcetlt", predef_wcetlt] in
  Iterenv.step(Iterenv.step l)

let last_tag = ref (-1)

let new_tag () =
  incr last_tag; !last_tag

let numerate_vars vdecl_list =
  let order = ref 0 in
  List.map (fun v -> order := !order + 1; {v with var_order = !order}) vdecl_list

let expr_of_ident id loc =
  {expr_tag = new_tag ();
   expr_desc = Expr_ident id;
   expr_type = Types.new_var ();
   expr_clock = Clocks.new_var true;
   expr_loc = loc}

let expr_list_of_expr expr =
  match expr.expr_desc with
  | Expr_tuple elist ->
      elist
  | _ -> [expr]

let pp_decl_type tdecl =
  match tdecl.top_decl_desc with
  | Node nd ->
      print_string nd.node_id;
      print_string ": ";
      Utils.reset_names ();
      Types.print_ty nd.node_type;
      print_newline ()
  | ImportedNode ind ->
      print_string ind.nodei_id;
      print_string ": ";
      Utils.reset_names ();
      Types.print_ty ind.nodei_type;
      print_newline ()
  | SensorDecl _ | ActuatorDecl _ | ConstDecl _ -> ()

let pp_prog_type tdecl_list =
  Utils.pp_list tdecl_list pp_decl_type "" "" ""

let pp_decl_clock cdecl =
  match cdecl.top_decl_desc with
  | Node nd ->
      print_string nd.node_id;
      print_string ": ";
      Utils.reset_names ();
      Clocks.print_ck nd.node_clock;
      print_newline ()
  | ImportedNode ind ->
      print_string ind.nodei_id;
      print_string ": ";
      Utils.reset_names ();
      Clocks.print_ck ind.nodei_clock;
      print_newline ()
  | SensorDecl _ | ActuatorDecl _ | ConstDecl _ -> ()

let pp_prog_clock prog =
  Utils.pp_list prog pp_decl_clock "" "" ""

let pp_error = function
    Main_not_found ->
      print_string "Cannot compile node ";
      print_string !Options.main_node;
      print_string ": could not find the node definition.";
      print_newline ()
  | Main_wrong_kind ->
      print_string "Name ";
      print_string !Options.main_node;
      print_string " does not correspond to a (non-imported) node definition.";
      print_newline ()
  | No_main_specified ->
      print_string "No main node specified (use option -node)";
      print_newline ()

let is_IN topd =
  match topd.top_decl_desc with
  | ImportedNode _ -> true
  | _              -> false

let get_IN_list prog =
  List.filter is_IN prog

let mystring_of_bool b =
  if !Options.bool_is_stdbool then
    if b then "true" else "false"
  else
    if b then "1" else "0"

let string_of_constant c =
  match c with
  | Const_int i -> string_of_int i
  | Const_char c -> "\'"^(Char.escaped c)^"\'"
  | Const_float f -> string_of_float f
  | Const_bool b -> mystring_of_bool b

let string_of_atom a =
  match a with
  | AConst_int i -> string_of_int i
  | AConst_char c -> "\'"^(Char.escaped c)^"\'"
  | AConst_float f -> string_of_float f
  | AConst_bool b -> mystring_of_bool b
  | AIdent i -> i

let is_atom_zero a =
  match a with
  | AConst_int 0 | AConst_float 0. | AConst_bool false -> true
  | AIdent _ -> failwith "Internal error: requested is_atom_zero for AIdent"
  | _ -> false
