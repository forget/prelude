(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generic inference environments. Used both for typing and
    clock-calculus. *)

open Utils

(** We have a generic and a constants-specific namespace *)
let initial = (IMap.empty, IMap.empty)

let add_value_local (l,g) ident v =
  IMap.add ident v l, g

let add_value_global (l,g) ident v =
  l,IMap.add ident v g

let lookup_value_local (l,_) ident =
  IMap.find ident l

let lookup_value_global (_,g) ident =
  IMap.find ident g

let lookup_value env ident =
  try
    lookup_value_local env ident
  with Not_found ->
    lookup_value_global env ident

let exists_value_local (l,_) ident =
  IMap.mem ident l

let exists_value_global (_,g) ident =
  IMap.mem ident g

let exists_value env ident =
  (exists_value_local env ident)
  ||
  (exists_value_global env ident)

open Format

let pp (l,g) pp_fun =
  let (llid,llty) = list_of_imap l in
  let (glib,glty) = list_of_imap g in
  let (lid,lty) = llid@glib, llty@glty in
  let l' = List.combine lid lty in
  let pp_fun (id,value) =
    print_string id;
    print_string " |-> ";
    pp_fun value
  in
  pp_list l' pp_fun "" "" "\n"
