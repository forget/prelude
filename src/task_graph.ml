(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Build the task graph (dependent task set) corresponding to a node. *)

open Format
open Corelang

type vertex_id = string

(** Predefined operators, ie rate transitions and all that. Note that
    Merge is treated as a task. *)
type predef_op =
  | Gfby of atom
  | Guclock of int
  | Gdclock of int
  | Gphclock of rat
  | Gtail
  | Gconcat of atom
  | Gwhen of vertex_id
  | Gwhennot of vertex_id

type prec_annot = predef_op list

type var_kind = RegularVar | WhenVar | WhennotVar

type node_kind = RegularNode | MergeNode | WCETNode of ident*int

type ndvar_desc =
    {ndvar_tag: tag;
     ndvar_var_decl: var_decl;
     ndvar_node_desc: imported_node_desc;
     ndvar_clock: Clocks.clock_expr;
     ndvar_kind: var_kind;
     ndvar_nd_kind: node_kind}

type predefvar_desc =
    {predefvar_tag: tag;
     predefvar_op: predef_op;
     predefvar_var_id: string}

type vertex_desc =
  | Constant of ident * tag
  | Var of var_decl
  | NodeVar of ndvar_desc
  | PredefOpVar of predefvar_desc

(** Code the graph with fast access adjacency lists (hashtables). Store
    both the predecessor and successors of a vertex. As we consider
    precedences between variables, each vertex can only have one
    predecessor (single assignement). *)

type vertex =
    {vertex_desc: vertex_desc;
     mutable vertex_pred: (vertex_id*prec_annot) option;
     mutable vertex_succs: (vertex_id, prec_annot) Hashtbl.t}

type t = (vertex_id, vertex) Hashtbl.t

type error =
    Main_variable_unused of ident

exception Error of Location.t * error

(** {4 Pretty printing } *)

let pp_predef op =
  match op with
  | Gfby init ->
      print_string "fby"
  | Guclock k ->
      print_string "*^";
      print_int k
  | Gdclock k ->
      print_string "/^";
      print_int k
  | Gphclock q ->
      print_string "~>";
      Utils.print_rat q
  | Gtail ->
      print_string "tail"
  | Gconcat _ ->
      print_string "concat"
  | Gwhen c ->
      print_string "when "; print_string c
  | Gwhennot c ->
      print_string "whennot "; print_string c

let pp_inst_id (ident,tag) =
  print_string ident;
  print_string "(";
  print_int tag;
  print_string ")"

let pp_vertex_desc v =
  match v with
  | Constant (cst,tag) ->
      pp_inst_id (cst,tag)
  | Var v ->
      print_string v.var_id
  | NodeVar ndesc ->
      pp_inst_id (ndesc.ndvar_node_desc.nodei_id,ndesc.ndvar_tag);
      print_string "_";
      print_string ndesc.ndvar_var_decl.var_id
  | PredefOpVar predef_desc ->
      pp_predef predef_desc.predefvar_op;
      print_string "(";
      print_int predef_desc.predefvar_tag;
      print_string ").";
      print_string predef_desc.predefvar_var_id


let pp_annot alist =
  Utils.pp_list alist pp_predef " -" "-> " "."

let pp_graph g =
  Hashtbl.iter
    (fun vid v ->
      Hashtbl.iter
        (fun sid annot ->
          print_string vid;
          pp_annot annot;
          print_string sid;
          print_newline ())
        v.vertex_succs)
    g

let pp_error = function
    Main_variable_unused id ->
      print_string ("Variable "^id^" is unused");
      print_newline ()

let predef_id name tag = name^(string_of_int tag)
let fby_id tag = predef_id "fby" tag
let uclock_id tag = predef_id "uclock" tag
let dclock_id tag = predef_id "dclock" tag
let phclock_id tag = predef_id "phclock" tag
let tail_id tag = predef_id "tail" tag
let concat_id tag = predef_id "concat" tag
let constant_id cst tag = predef_id (string_of_constant cst) tag

let when_id tag vid = "when"^(string_of_int tag)^"_"^vid
let whennot_id tag vid = "whennot"^(string_of_int tag)^"_"^vid

let nodevar_id nid ntag vid = nid^(string_of_int ntag)^"_"^vid

let id_of_pred p =
  match p with
  | None -> failwith "Internal error: id_of_pred"
  | Some (id,_) -> id

let new_graph () = Hashtbl.create 30

let new_vertex vdesc =
  {vertex_desc = vdesc;
   vertex_pred = None;
   vertex_succs = Hashtbl.create 10}

let new_node_var nd var tag ck var_kind node_kind =
  {ndvar_tag = tag;
   ndvar_var_decl = var;
   ndvar_node_desc = nd;
   ndvar_clock = ck;
   ndvar_kind = var_kind;
   ndvar_nd_kind = node_kind}

let new_predef_var predef_op tag var_id =
  PredefOpVar
    {predefvar_tag = tag;
     predefvar_op = predef_op;
     predefvar_var_id = var_id}

let new_unary_predef predef_op tag =
  new_predef_var predef_op tag "v"

let vdecl_of_vid nd vid =
  let vars = nd.node_inputs@nd.node_outputs@nd.node_locals in
  List.find (fun v -> v.var_id=vid) vars

let vdecl_of_vdesc vdesc =
  match vdesc with
  | Var v -> v
  | NodeVar ndesc -> ndesc.ndvar_var_decl
  | _ -> failwith "Internal error: vdecl_of_vertex"

(** The number of precedences of a graph *)
let nb_precs g =
  Hashtbl.fold (fun _ v cpt -> cpt+(Hashtbl.length v.vertex_succs)) g 0

(** {4 Graph construction, starting from a single (main) node } *)

(** [add_prec g from_v to_v] adds a precedence [from_v]->[to_v]. Adds the
    vertices if they are not already elements of the graph*)
let add_prec g annot (from_vid,from_vdesc) (to_vid,to_vdesc) =
  let from_v =
    try
      Hashtbl.find g from_vid
    with Not_found ->
      let v = new_vertex from_vdesc in
      Hashtbl.add g from_vid v;
      v
  in
  Hashtbl.add from_v.vertex_succs to_vid annot;
  let to_v =
    try
      Hashtbl.find g to_vid
    with Not_found ->
      let v = new_vertex to_vdesc in
      Hashtbl.add g to_vid v;
      v
  in
  to_v.vertex_pred <- (Some (from_vid,annot))

let is_delayed_prec prec_annot =
  List.exists (fun a -> match a with Gfby _ -> true | _ -> false) prec_annot

let is_output v =
  Hashtbl.length v.vertex_succs <> 0

let is_input v =
  v.vertex_pred <> None

let contains_init prec_annot =
  List.exists (fun a -> match a with Gfby _ | Gconcat _ -> true |_ -> false) prec_annot

let impnode_of_id id env =
  let nd = Iterenv.get env id in
  match nd.top_decl_desc with
  | ImportedNode ind -> ind
  | Node _ | SensorDecl _ | ActuatorDecl _ | ConstDecl _ -> failwith "Internal error impnode_of_id"

(** [no_succs expr] returns the vertices of [expr] that have no
    successors *)
let rec no_succs exp_main env expr =
  match expr.expr_desc with
  | Expr_atom a -> failwith "Internal error no_succs"
  | Expr_const cst -> [(constant_id cst expr.expr_tag),Constant ((string_of_constant cst), expr.expr_tag)]
  | Expr_ident id -> [id,Var (vdecl_of_vid exp_main id)]
  | Expr_tuple elist -> List.flatten (List.map (no_succs exp_main env) elist)
  | Expr_fby (cst,_) ->
      [(fby_id expr.expr_tag), new_unary_predef (Gfby cst) expr.expr_tag]
  | Expr_uclock (_,k) ->
      [(uclock_id expr.expr_tag), new_unary_predef (Guclock k) expr.expr_tag]
  | Expr_dclock (_,k) ->
      [(dclock_id expr.expr_tag), new_unary_predef (Gdclock k) expr.expr_tag]
  | Expr_phclock (_,q) ->
      [(phclock_id expr.expr_tag), new_unary_predef (Gphclock q) expr.expr_tag]
  | Expr_tail _ ->
      [(tail_id expr.expr_tag), new_unary_predef Gtail expr.expr_tag]
  | Expr_concat (cst,_) ->
      [(concat_id expr.expr_tag), new_unary_predef (Gconcat cst) expr.expr_tag]
  | Expr_appl (id,_) ->
      let ind = impnode_of_id id env in
      List.map
        (fun v ->
          let nid = nodevar_id ind.nodei_id expr.expr_tag v.var_id in
          let ndesc = NodeVar (new_node_var ind v expr.expr_tag expr.expr_clock RegularVar RegularNode) in
          nid,ndesc)
        ind.nodei_outputs
  | Expr_merge (c,e1,e2) ->
      let ind = impnode_of_id "merge" env in
      List.map
        (fun v ->
          let nid = nodevar_id ind.nodei_id expr.expr_tag v.var_id in
          let typed_v = {v with var_type=expr.expr_type; var_clock=expr.expr_clock} in
          let ndesc = NodeVar (new_node_var ind typed_v expr.expr_tag expr.expr_clock RegularVar MergeNode) in
          nid,ndesc)
        ind.nodei_outputs
  | Expr_when (e,c) ->
      [(when_id expr.expr_tag "o"), new_predef_var (Gwhen c) expr.expr_tag "o"]
  | Expr_whennot (e,c) ->
      [(whennot_id expr.expr_tag "o"), new_predef_var (Gwhennot c) expr.expr_tag "o"]
  | Expr_rate (e,_) -> no_succs exp_main env e
  | Expr_wcetlt (id,_,n) ->
      let wcetnd = impnode_of_id "wcetlt" env in
      List.map
        (fun v ->
          let nid = nodevar_id "wcetlt" expr.expr_tag v.var_id in
          let ndesc = NodeVar (new_node_var wcetnd v expr.expr_tag expr.expr_clock RegularVar (WCETNode (id,n))) in
          nid,ndesc)
        wcetnd.nodei_outputs
  | Expr_map (id,n,_) -> failwith "TODO"

let join_precs g from_vs to_vs =
  match from_vs with
  | [] -> ()
  | _ -> List.iter2 (add_prec g []) from_vs to_vs

(** [of_expr g expr] adds the vertices and edges corresponding to [expr]
in graph [g] *)
let rec of_expr g exp_main env expr =
  match expr.expr_desc with
  | Expr_atom _ -> failwith "Internal error of_expr"
  | Expr_const _ -> ()
  | Expr_ident id -> () (* Vertex added by precedences related to it *)
  | Expr_tuple elist -> List.iter (of_expr g exp_main env) elist
  | Expr_fby (cst,e) ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Gfby cst) expr.expr_tag in
      add_prec g [] last (fby_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_uclock (e,k) ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Guclock k) expr.expr_tag in
      add_prec g [] last (uclock_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_dclock (e,k) ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Gdclock k) expr.expr_tag in
      add_prec g [] last (dclock_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_phclock (e,q) ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Gphclock q) expr.expr_tag in
      add_prec g [] last (phclock_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_tail e ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Gtail) expr.expr_tag in
      add_prec g [] last (tail_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_concat (cst,e) ->
      let last = List.hd (no_succs exp_main env e) in
      let vdesc = new_unary_predef (Gconcat cst) expr.expr_tag in
      add_prec g [] last (concat_id expr.expr_tag,vdesc);
      of_expr g exp_main env e
  | Expr_appl (id,e) ->
      let lasts = no_succs exp_main env e in
      let ind = impnode_of_id id env in
      let vins =
        List.map
          (fun v ->
            let nid = nodevar_id ind.nodei_id expr.expr_tag v.var_id in
            let ndesc = NodeVar (new_node_var ind v expr.expr_tag expr.expr_clock RegularVar RegularNode) in
          nid,ndesc)
          ind.nodei_inputs in
      join_precs g lasts vins;
      of_expr g exp_main env e
  | Expr_merge (c,e1,e2) ->
      let ind = impnode_of_id "merge" env in
      let (var_c,var_i1,var_i2 as ins) = match ind.nodei_inputs with
      | [var_c;var_i1;var_i2] -> var_c,var_i1,var_i2
      | _ -> failwith "Internal error: of_expr" in
      let c_nid = nodevar_id ind.nodei_id expr.expr_tag var_c.var_id in
      let c_typed = {var_c with var_clock=expr.expr_clock} in
      let c_desc = NodeVar (new_node_var ind c_typed expr.expr_tag expr.expr_clock RegularVar MergeNode) in
      let i1_nid = nodevar_id ind.nodei_id expr.expr_tag var_i1.var_id in
      let i1_typed = {var_i1 with var_type=e1.expr_type; var_clock=e1.expr_clock} in
      let i1_desc = NodeVar (new_node_var ind i1_typed expr.expr_tag e1.expr_clock RegularVar MergeNode) in
      let i2_nid = nodevar_id ind.nodei_id expr.expr_tag var_i2.var_id in
      let i2_typed = {var_i2 with var_type=e2.expr_type; var_clock=e2.expr_clock} in
      let i2_desc = NodeVar (new_node_var ind i2_typed expr.expr_tag e2.expr_clock RegularVar MergeNode) in
      let last_c = (c,Var (vdecl_of_vid exp_main c)) in
      add_prec g [] last_c (c_nid,c_desc);
      let last_e1 = List.hd (no_succs exp_main env e1) in
      add_prec g [] last_e1 (i1_nid,i1_desc);
      let last_e2 = List.hd (no_succs exp_main env e2) in
      add_prec g [] last_e2 (i2_nid,i2_desc);
      of_expr g exp_main env e1; of_expr g exp_main env e2
  | Expr_when (e,c) ->
      let c_id = when_id expr.expr_tag "c" in
      let c_var = new_predef_var (Gwhen c) expr.expr_tag "c" in
      let last_c = (c,Var (vdecl_of_vid exp_main c)) in
      add_prec g [] last_c (c_id,c_var);
      let e_id = when_id expr.expr_tag "e" in
      let e_var = new_predef_var (Gwhen c) expr.expr_tag "e" in
      let last_e = List.hd (no_succs exp_main env e) in
      add_prec g [] last_e (e_id,e_var);
      of_expr g exp_main env e
  | Expr_whennot (e,c) ->
      let c_id = whennot_id expr.expr_tag "c" in
      let c_var = new_predef_var (Gwhennot c) expr.expr_tag "c" in
      let last_c = (c,Var (vdecl_of_vid exp_main c)) in
      add_prec g [] last_c (c_id,c_var);
      let e_id = whennot_id expr.expr_tag "e" in
      let e_var = new_predef_var (Gwhennot c) expr.expr_tag "e" in
      let last_e = List.hd (no_succs exp_main env e) in
      add_prec g [] last_e (e_id,e_var);
      of_expr g exp_main env e
  | Expr_rate (e,_) -> of_expr g exp_main env e
  | Expr_wcetlt (id,e,n) ->
      let lasts = no_succs exp_main env e in
      let ind = impnode_of_id id env in
      let wcetnd = impnode_of_id "wcetlt" env in
      let vins =
        List.map
          (fun v ->
            let nid = nodevar_id "wcetlt" expr.expr_tag v.var_id in
            let ndesc = NodeVar (new_node_var wcetnd v expr.expr_tag expr.expr_clock RegularVar (WCETNode (id,n))) in
          nid,ndesc)
          ind.nodei_inputs in
      join_precs g lasts vins;
      of_expr g exp_main env   e
  | Expr_map (id,n,e) ->
      failwith "TODO"

(** [of_eq g eq] adds the vertices and edges corresponding to [eq]
in graph [g] *)
let of_eq g exp_main env eq =
  of_expr g exp_main env eq.eq_rhs;
  let lasts = no_succs exp_main env eq.eq_rhs in
  let vdecls = List.map (fun vid -> (vid,Var (vdecl_of_vid exp_main vid))) eq.eq_lhs in
  join_precs g lasts vdecls

(** [of_node exp_main] returns the task graph corresponding to main node [exp_main] *)
let of_node exp_main env =
  let g = new_graph () in
  List.iter (of_eq g exp_main env) exp_main.node_eqs;
  g

(** {4 Graph reduction. Removes local variables and transforms
   predefined operators into precedence annotations } *)

(** [transfer_succs g v] maps the predecessor of variable [v] to its
    successors. To do before reducing annotations. *)
let transfer_succs g vdecl =
  let vid = vdecl.var_id in
  let v = Hashtbl.find g vid in
  begin
    match v.vertex_pred with
    | None -> ()
    | Some (pred,_) ->
        let succs_pred = (Hashtbl.find g pred).vertex_succs in
        Hashtbl.remove succs_pred vid;
        Hashtbl.iter
          (fun succid _ ->
            let succ = Hashtbl.find g succid in
            succ.vertex_pred <- Some (pred,[]);
            Hashtbl.add succs_pred succid [])
          v.vertex_succs
  end

(** [remove_vertex g v] removes vertex [v] from graph [g].  *)
let remove_vertex g v =
  transfer_succs g v;
  Hashtbl.remove g v.var_id

(** [remove_local_vars p] removes all the local variables of graph [g]. *)
let remove_local_vars g nd =
  List.iter (fun v -> remove_vertex g v) nd.node_locals

let transfer_outputs_succs g nd =
  List.iter (fun v -> transfer_succs g v) nd.node_outputs

let add_prec_and_vertex g prec_annot from_v (to_vid,to_vdesc) =
  match from_v with
  | None ->
      if not (Hashtbl.mem g to_vid) then
        let to_v = new_vertex to_vdesc in
        Hashtbl.add g to_vid to_v
  | Some from_v ->
      add_prec g prec_annot from_v (to_vid,to_vdesc)

(* Could be optimized (made tail-recursive) *)
let rec reduce_pred g p =
  match p with
  | None -> (None, [], [])
  | Some (vid,_) ->
      let v = Hashtbl.find g vid in
      match v.vertex_desc with
      | Var _ | NodeVar _ | Constant _ -> Some (vid,v.vertex_desc), [], []
      | PredefOpVar predef_desc ->
          match predef_desc.predefvar_op with
          | Gwhen _ ->
              let v_c = Hashtbl.find g (when_id predef_desc.predefvar_tag "c") in
              let cond_pred, cond_annot, cond_conds = reduce_pred g v_c.vertex_pred in
              let cond_pred_id = id_of_pred cond_pred in
              let v_e = Hashtbl.find g (when_id predef_desc.predefvar_tag "e") in
              let pred_rec,precannot,conds = reduce_pred g v_e.vertex_pred in
              pred_rec,precannot@[(Gwhen cond_pred_id)],((cond_pred,WhenVar,cond_annot)::cond_conds@conds)
          | Gwhennot _ ->
              let v_c = Hashtbl.find g (whennot_id predef_desc.predefvar_tag "c") in
              let cond_pred, cond_annot, cond_conds = reduce_pred g v_c.vertex_pred in
              let cond_pred_id = id_of_pred cond_pred in
              let v_e = Hashtbl.find g (whennot_id predef_desc.predefvar_tag "e") in
              let pred_rec,precannot,conds = reduce_pred g v_e.vertex_pred in
              pred_rec,precannot@[(Gwhennot cond_pred_id)],((cond_pred,WhennotVar,cond_annot)::cond_conds@conds)
          | _ ->
              let first_pred = v.vertex_pred in
              let pred_rec, precannot, conds = reduce_pred g first_pred in
              pred_rec, precannot@[predef_desc.predefvar_op],conds

let reduce g exp_main =
  remove_local_vars g exp_main;
  transfer_outputs_succs g exp_main;
  let reduced = Hashtbl.create 30 in
  Hashtbl.iter
    (fun vid v ->
      match v.vertex_desc with
      | Var _ ->
          (* mains vars cannot be conditionned *)
          let new_pred, predannot, _ = reduce_pred g v.vertex_pred in
          add_prec_and_vertex reduced predannot new_pred (vid,v.vertex_desc)
      | NodeVar ndesc ->
          begin
            let new_pred, predannot, conds = reduce_pred g v.vertex_pred in (* conds will be a list of (cond_var,annots) *)
            add_prec_and_vertex reduced predannot new_pred (vid,v.vertex_desc);
            let conds =
              if ndesc.ndvar_nd_kind = MergeNode &&
                ndesc.ndvar_var_decl.var_id <> "c" && conds <> []
              then List.tl conds
              else conds in
            List.iter
              (fun (cond,cond_kind,cond_annot) ->
                match cond with
                | None -> ()
                | Some (cond_id,cond_desc) ->
                    let to_vid = nodevar_id ndesc.ndvar_node_desc.nodei_id ndesc.ndvar_tag cond_id in
                    let to_vdesc = NodeVar {ndesc with ndvar_kind = cond_kind; ndvar_var_decl =
                                            {(vdecl_of_vdesc cond_desc) with
                                             var_clock = ndesc.ndvar_clock;
                                             var_id=cond_id}
                                          } in
                    add_prec_and_vertex reduced cond_annot (Some (cond_id,cond_desc)) (to_vid,to_vdesc))
              conds
          end
      | PredefOpVar _ | Constant _ -> ())
    g;
  reduced

(** {4 Check graph topology. For now, only checks that no main node input/local is unused } *)

let check_variables_unused g exp_main =
  List.iter (fun vin ->
    let vid = vin.var_id in
    if not (Hashtbl.mem g vid)
    then raise (Error (vin.var_loc, Main_variable_unused vid)))
    (exp_main.node_inputs@exp_main.node_locals);
  List.iter (fun vlocal ->
    let vid = vlocal.var_id in
    let v = Hashtbl.find g vid in
    if ((Hashtbl.length v.vertex_succs)=0) then
      raise (Error (vlocal.var_loc, Main_variable_unused vid)))
    exp_main.node_locals

let reduced_task_graph exp_main env =
  let env = Iterenv.skip_until env exp_main.node_id in
  let g = of_node exp_main env in
  check_variables_unused g exp_main;
  reduce g exp_main

let same_task v1 v2 =
  match v1,v2 with
  | Var id1, Var id2 -> id1=id2 (* Actuator/Sensor *)
  | Constant (_,t1), Constant (_,t2) -> t1=t2
  | NodeVar nd1, NodeVar nd2 -> (nd1.ndvar_node_desc.nodei_id = nd2.ndvar_node_desc.nodei_id) &&
      (nd1.ndvar_tag = nd2.ndvar_tag)
  | _,_ -> false


(** The list of all precedences of a graph. WARNING: this is a list of
    precedences, not dependences. A precedence relates tasks, not
    variables. *)
let prec_list g =
  let precs =
    Hashtbl.fold
      (fun vid v precs ->
        match (v.vertex_desc, !Options.io_code) with
        | (Constant _,_) | (Var _, false) ->
          precs
        | _ ->
            let more_precs =
              Hashtbl.fold
                (fun succ_id annot l ->
                  let succ = Hashtbl.find g succ_id in
                  match (succ.vertex_desc, !Options.io_code) with
                    (Var _, false) -> l
                  | (_,_) -> (v,annot,succ)::l)
                v.vertex_succs [] in
            precs@more_precs)
      g []
  in
  (* We need to filter precs to keep only one prec by pair of
     (src_task,dst_task) *)
  let prec_exists vsrc vdst l =
    List.exists
      (fun (vsrc',_,vdst') ->
        (same_task vsrc.vertex_desc vsrc'.vertex_desc) &&
        (same_task vdst.vertex_desc vdst'.vertex_desc))
      l
  in
  let rec filter_dup_precs l =
  match l with
  | [] -> []
  | ((vsrc,_,vdst) as prec)::tl ->
      if prec_exists vsrc vdst tl then
        filter_dup_precs tl
      else
        prec::(filter_dup_precs tl)
  in
  filter_dup_precs precs

(** The period of a vertex *)
let get_period v =
  let c = match v.vertex_desc with
    | Var v -> Clocks.pclock_parent v.var_clock
    | NodeVar nd -> Clocks.pclock_parent (Clocks.clock_of_impnode_clock nd.ndvar_clock)
    | _ -> failwith "Task_graph get_period"
  in
  Clocks.period c

(** Hyperperiod of vertices src and dst *)
let hyperperiod src dst =
  let c1 = get_period src in
  let c2 = get_period dst in
  Utils.lcm c1 c2
