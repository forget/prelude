(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate declarations of C communication variables. *)

open Corelang
open Task_set
open C_utils
open Format

(** Tuple types for tasks with multiple outputs. We need to know if this
    type is already declared in the case where the same node is instanciated
    several times in the same program. *)
let declared_tuple_types = Hashtbl.create 30

(** Declare a struct type for the set of outputs of [task] *)
let pp_tuple_type types_f task =
  let task_name,_ = task.task_id in
  if not (Hashtbl.mem declared_tuple_types task_name) then
    begin
      Hashtbl.add declared_tuple_types task_name ();
      fprintf types_f "@[<v 2>%s {@ " (outstruct_type task.task_id);
      Print.list_printer_from_printer "" "@ " "@]@ };@ @ "
        (fun types_f (outv,_) ->
          fprintf types_f "%s;" (string_of_type_var outv.var_type outv.var_id))
        types_f task.task_outputs
    end

(** Declare a buffer for communication [vref_from->vref_to] *)
let pp_buffer_alloc out_f task ty read_proto vref_from vref_to =
  match vref_from with
  | ConstVal _ -> () (* No communication for a const value *)
  | _ ->
      let bname = combuffer_name vref_from vref_to in
      let bdim = dim_string ty in
      let ty_str = tystring_of_type ty in
      let buf_size = read_proto.rbuf_size in
      if Options.trace_instances () then
        if buf_size > 1 then
          begin
            fprintf out_f "struct { %s value; int instance; } %s%s[%i]" ty_str bname bdim buf_size;
            match read_proto.rbuf_init with
            | Some a ->
                fprintf out_f "={";
                for i=0 to buf_size -2 do
                  fprintf out_f "{%s, -1}, " (string_of_atom a)
                done;
                fprintf out_f "{%s, -1}};@ " (string_of_atom a)
            | None -> fprintf out_f ";@ "
          end
        else
          fprintf out_f "struct { %s value; int instance; } %s%s;@ " ty_str bname bdim
      else
        if buf_size > 1 then
          begin
            fprintf out_f "%s %s%s[%i]" ty_str bname bdim buf_size;
            match read_proto.rbuf_init with
            | Some a ->
                fprintf out_f "=";
                print_array_litteral out_f
                  (fun out_f v -> fprintf out_f "%s" (string_of_atom v))
                  (zero_atom_string a)
                  (fun v -> is_atom_zero v)
                  (Array.make buf_size a);
                fprintf out_f ";@ "
            | None -> fprintf out_f ";@ "
          end
        else
          fprintf out_f "%s %s%s;@ " ty_str bname bdim

(** Declare communication buffers for the inputs and activation conditions of [task] *)
let pp_buffer_allocs_task out_f task =
  List.iter
    (fun (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      pp_buffer_alloc out_f task vin.var_type read_proto vref_from vref_to)
    task.task_inputs;
  List.iter
    (fun cond ->
      let vref_to = vref_of_task_var task cond.cond_var in
      let vref_from,read_proto = cond.cond_pred in
      pp_buffer_alloc out_f task cond.cond_var.var_type read_proto vref_from vref_to)
    task.task_conds

(** Declare communication buffers for the whole [task_set] *)   
let pp_buffer_allocs out_f task_set =
  Hashtbl.iter (fun _ task -> pp_buffer_allocs_task out_f task) task_set;
  fprintf out_f "@ "

(** Declare local input variables for [task] *)   
let pp_lread_allocs_task out_f task static =
  List.iter (fun (vin,_,_) -> 
    let vname = local_rbuffer_name task.task_id vin.var_id in
    if static then fprintf out_f "static ";
    fprintf out_f "%s;@ " (string_of_type_var vin.var_type vname))
    task.task_inputs

(** Declare local input variables for the whole [task_set] *)   
let pp_lread_allocs out_f task_set =
  Hashtbl.iter (fun _ task -> pp_lread_allocs_task out_f task true) task_set

(** Declare local output variables for [task] *)      
let pp_lwrite_allocs_task out_f types_f task static =
  if (List.length task.task_outputs >1)
  then (* use a struct *)
    begin
      if static then
        fprintf out_f "static ";
      pp_tuple_type types_f task;
      fprintf out_f "%s %s;@ "
        (outstruct_type task.task_id)
        (outstruct_name task.task_id)
    end
  else if List.length task.task_outputs = 1 then (* use a scalar *)
    begin
      if static then
        fprintf out_f "static ";
      let vout,_ = List.hd task.task_outputs in
      let vname = local_wbuffer_name task.task_id vout.var_id in
      fprintf out_f "%s;@ " (string_of_type_var vout.var_type vname)
    end

(** Declare local output variables for the whole [task_set] *)      
let pp_lwrite_allocs out_f types_f task_set =
  Hashtbl.iter (fun _ task -> pp_lwrite_allocs_task out_f types_f task true) task_set

(** Print an array*)
let pp_array out_f ty_str name length =
  fprintf out_f "%s %s[%i];@ " ty_str name length

(** Generic protocol printer*)
let pp_proto_struct out_f struct_type proto_name no_init tid vid pref pat =
  let pref_length, pat_length =
    (Array.length pref),
    (Array.length pat) in
  if pref_length > 1 || pat_length > 1 then
    begin
      fprintf out_f "static struct %s %s" struct_type proto_name;
      if no_init then
        begin
          fprintf out_f ";@ ";
          if pref_length > 0 then
            pp_array out_f "static int" (proto_array_name proto_name true) pref_length;
          if pat_length > 0 then
            pp_array out_f "static int" (proto_array_name proto_name false) pat_length;
        end
      else
        begin
          fprintf out_f " =@ {";
          (* Prefix *)
          if pref_length > 0 then
            begin
              fprintf out_f "(int[%d])" pref_length;
              print_array_litteral out_f
                (fun out_f v -> fprintf out_f "%s" (cstring_of_bool v))
                (cstring_of_bool false)
                (fun v -> v=false)
                pref
            end
          else (* no prefix protocol needed *)
            fprintf out_f "NULL";
          fprintf out_f ", %i, " pref_length;
          if pat_length > 0 then
            begin
              fprintf out_f "(int[%d])" pat_length;
              print_array_litteral out_f
                (fun out_f v -> fprintf out_f "%s" (cstring_of_bool v))
                (cstring_of_bool false)
                (fun v -> v=false)
                pat
            end
          else (* no pattern protocol needed *)
            fprintf out_f "NULL";
          fprintf out_f ", %i};@ " pat_length
        end
      end

(** Produce the read communication protocol for communication [vref_pred->vin] *)
let pp_read_proto out_f tid no_init (vin,vref_pred,proto) =
  if proto.rbuf_size > 1 then
    pp_proto_struct out_f
      read_proto_struct_name
      (read_proto_name tid vin.var_id)
      no_init
      tid
      vin.var_id
      proto.change_pref
      proto.change_pat
  
(** Produce the write communication protocol for communication [vout->vref_succ] *)
let pp_write_proto out_f tid vout vref_succ proto no_init =
  if proto.wbuf_size > 1 then
    pp_proto_struct out_f
      write_proto_struct_name
      (write_proto_name tid vout.var_id vref_succ)
      no_init
      tid
      vout.var_id
      proto.write_pref
      proto.write_pat
        
(** Produce the write communication protocols for all the [readers] of [vout] *)
let pp_write_protos out_f tid no_init (vout,readers) =
  List.iter
    (fun (vref_succ,proto) -> pp_write_proto out_f tid vout vref_succ proto no_init)
    readers

(** Produce all the communication protocols for the inputs and outputs of [task] *)
let pp_task_protocols out_f task no_init =
  List.iter (pp_read_proto out_f task.task_id no_init) task.task_inputs;
  List.iter (pp_write_protos out_f task.task_id no_init) task.task_outputs

(** Produce communication protocols for the whole [task_set] *)   
let pp_protocols out_f task_set no_init =
  Hashtbl.iter
    (fun _ t -> if t.task_kind <> CstTask then 
      pp_task_protocols out_f t no_init)
    task_set;
  fprintf out_f "@ "

(** Declare indices for access to buffers of [task] allocated in external
   memory. For multi-core architectures.*)
let pp_buffer_indices_task out_f task =
  List.iter
    (fun (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      fprintf out_f "%s,@ " (combuffer_id (combuffer_name vref_from vref_to)))
    task.task_inputs

(** Declare buffer indices for the whole [task_set] *)   
let pp_buffer_indices out_f task_set =
  fprintf out_f "@[<v 2>enum {@ ";
  Hashtbl.iter (fun _ task -> pp_buffer_indices_task out_f task) task_set;
  fprintf out_f "%s@]@ };@ @ " nb_buffers_name

(** Generate buffer addresses for [task]. For buffers in extern memory. *)
let pp_buffer_addr_task out_f task =
  Print.list_printer_from_printer "" "@ " "" 
    (fun out_f (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      fprintf out_f " (void *) ";
      if read_proto.rbuf_size = 1 then
        fprintf out_f "&";
      fprintf out_f "%s," (combuffer_name vref_from vref_to);
    )
    out_f task.task_inputs

(** Generate buffer addresses for the whole [task_set]. For buffers in extern memory. *)    
let pp_buffer_addrs out_f task_set =
  fprintf out_f "void * buffer_addresses [%s] =@ " nb_buffers_name;
  Print.hashtbl_printer_from_printer "@[<v 2>{@ " "@ " "@]@ };"
    (fun ouf_f (_,task) -> pp_buffer_addr_task out_f task)
    out_f task_set;
  fprintf out_f "@."

(** Allocate extern buffers and gather their addresses. *)    
let pp_extern_buffers out_f task_set =
  pp_buffer_allocs out_f task_set;
  pp_buffer_addrs out_f task_set
