(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** *)

open Clocks
open Corelang

(** Applies variable substitutions *)
let subst_var var_substs vid =
  try Hashtbl.find var_substs vid with Not_found -> vid

(** Applies clock substitutions *)
let rec subst_ck ck_substs var_substs ck =
  match ck.cdesc with
  | Carrow (ck1,ck2) ->
      {ck with cdesc =
       Carrow (subst_ck ck_substs var_substs ck1,
               subst_ck ck_substs var_substs ck2)}
  | Ctuple cklist ->
      {ck with cdesc =
       Ctuple (List.map (subst_ck ck_substs var_substs) cklist)}
  | Con (ck',c) ->
      {ck with cdesc =
       Con (subst_ck ck_substs var_substs ck',c)}
  | Connot (ck',c) ->
      {ck with cdesc =
       Connot (subst_ck ck_substs var_substs ck',c)}
  | Pck_up (ck',k) ->
      {ck with cdesc =
       Pck_up (subst_ck ck_substs var_substs ck', k)}
  | Pck_down (ck',k) ->
      {ck with cdesc =
       Pck_down (subst_ck ck_substs var_substs ck', k)}
  | Pck_phase (ck',q) ->
      {ck with cdesc =
       Pck_phase (subst_ck ck_substs var_substs ck', q)}
  | Pck_const _ ->
      ck
  | Cvar _ | Cunivar _ ->
      begin
        try Hashtbl.find ck_substs ck with Not_found -> ck
      end
  | Clink ck' ->
      subst_ck ck_substs var_substs ck'

(** [new_expr_instance ck_substs var_substs e edesc] returns a new
    "instance" of expressions [e] of expression [e], where [edesc] is the
    expanded version of [e]. *)
let new_expr_instance ck_substs var_substs e edesc =
  {expr_tag = new_tag ();
   expr_desc = edesc;
   expr_type = e.expr_type;
   expr_clock = subst_ck ck_substs var_substs e.expr_clock;
   expr_loc = e.expr_loc}

let locals_cpt = ref 0

(** Returns a new local variable (for the main node) *)
let new_local old_vid vtyp vck vdd vloc =
  let vid = old_vid^"_"^(string_of_int !locals_cpt) in
  locals_cpt := !locals_cpt+1;
  let ty_dec = {ty_dec_desc = Tydec_any; ty_dec_loc = vloc} in (* dummy *)
  let ck_dec = {ck_dec_desc = Ckdec_any; ck_dec_loc = vloc} in (* dummy *)
  {var_id = vid;
   var_dec_type = ty_dec;
   var_dec_clock = ck_dec;
   var_dec_deadline = vdd;
   var_type = vtyp;
   var_clock = vck;
   var_loc = vloc;
   var_order = -1}

(** Returns an expression correponding to variable v *)
let expr_of_var v =
  {expr_tag = new_tag ();
   expr_desc = Expr_ident v.var_id;
   expr_type = v.var_type;
   expr_clock = v.var_clock;
   expr_loc = v.var_loc}

(** [build_ck_substs ck for_ck] computes the variable substitutions
    corresponding to the substitution of [ck] for [for_ck] *)
let build_ck_substs ck for_ck =
  let substs = Hashtbl.create 10 in
  let rec aux ck for_ck =
    let ck, for_ck = Clocks.repr ck, Clocks.repr for_ck in
    match ck.Clocks.cdesc, for_ck.Clocks.cdesc with
    | Clocks.Ctuple cklist1, Clocks.Ctuple cklist2 ->
        List.iter2 aux cklist1 cklist2
    | _, Clocks.Cunivar _ ->
        Hashtbl.add substs for_ck ck
    | _,_ ->
        ()
  in
  aux ck for_ck;
  substs

let nd_from_env env : node_desc =
  match Iterenv.next env with
  | {top_decl_desc = Node x} -> x
  | _ -> failwith "Iternal error nd_from_env"

(** Expands a list of expressions *)
let rec expand_list ck_substs var_substs env elist =
  List.fold_right
    (fun e (eqs,locs,elist) ->
      let eqs',locs',e' = expand_expr ck_substs var_substs env e in
      eqs'@eqs,locs'@locs,e'::elist)
    elist ([],[],[])

(** Expands the node instance [nd(args)]. *)
and expand_nodeinst parent_ck_substs parent_vsubsts penv env args =
  let nd = nd_from_env env in
  (* Expand arguments *)
  let args_eqs, args_locs, args' =
    expand_expr parent_ck_substs parent_vsubsts penv args in
  (* Compute clock substitutions to apply inside node's body *)
  let ck_ins = args'.expr_clock in
  let for_ck_ins,_ = Clocks.split_arrow nd.node_clock in
  let ck_substs = build_ck_substs ck_ins for_ck_ins in
  (* Compute variable substitutions to apply inside node's body, due
     to the transformation of node variables into local variables of the
     main node. *)
  let var_substs = Hashtbl.create 10 in
  (* Add an equation in=arg for each node input + transform node input
     into a local variable of the main node *)
  let eq_ins, loc_ins =
    List.split
      (List.map2
         (fun i e ->
           let i' =
             new_local i.var_id i.var_type i.var_clock i.var_dec_deadline i.var_loc in
           Hashtbl.add var_substs i.var_id i'.var_id;
           {eq_lhs = [i'.var_id];
            eq_rhs = e;
            eq_loc = i.var_loc}, i')
         nd.node_inputs (expr_list_of_expr args'))
  in
  (* Transform node local variables into local variables of the main
     node *)
  let loc_sub =
    List.map
      (fun v ->
        let v' = new_local v.var_id v.var_type v.var_clock v.var_dec_deadline v.var_loc in
        Hashtbl.add var_substs v.var_id v'.var_id;
        v')
      nd.node_locals
  in
  (* Same for outputs *)
  let loc_outs =
    List.map
      (fun o ->
        let o' = new_local o.var_id o.var_type o.var_clock o.var_dec_deadline o.var_loc in
        Hashtbl.add var_substs o.var_id o'.var_id;
        o')
      nd.node_outputs
  in
  (* A tuple made of the node outputs will replace the node call in the parent
     node *)
  let eout = Expr_tuple (List.map expr_of_var loc_outs) in
  let new_eqs, new_locals = expand_eqs ck_substs var_substs env nd.node_eqs in
  args_eqs@eq_ins@new_eqs,
  args_locs@loc_ins@loc_outs@loc_sub@new_locals,
  eout

and is_local id env =
  match Iterenv.next env with
  | {top_decl_desc = Node nd} ->
    List.exists
      (fun x -> x.var_id = id)
      (nd.node_inputs @ nd.node_locals @ nd.node_outputs)
  | _ -> failwith "Iternal error is_local"

and inline_atom a env =
  match a with
  | AConst_int _ | AConst_float _ | AConst_bool _ | AConst_char _ -> a
  | AIdent id ->
    try
      match (Iterenv.get env id).top_decl_desc with
      | ConstDecl {const_value = Const_int i; _} -> AConst_int i
      | ConstDecl {const_value = Const_float f; _} -> AConst_float f
      | ConstDecl {const_value = Const_bool b; _} -> AConst_bool b
      | _ -> failwith ("Internal error: expand.ml inline_atom "^id^" 1.")
    with Not_found ->
      failwith ("Internal error: expand.ml inline_atom "^id^" 1.")

(* Expands an expression *)
and expand_expr ck_substs var_substs env expr =
  match expr.expr_desc with
  | Expr_atom (AConst_int i) ->
    let e' = new_expr_instance ck_substs var_substs expr (Expr_const(Const_int i)) in
    expand_expr ck_substs var_substs env e'
  | Expr_atom (AConst_char c) ->
    let e' = new_expr_instance ck_substs var_substs expr (Expr_const(Const_char c)) in
    expand_expr ck_substs var_substs env e'      
  | Expr_atom (AConst_float f) ->
    let e' = new_expr_instance ck_substs var_substs expr (Expr_const(Const_float f)) in
    expand_expr ck_substs var_substs env e'
  | Expr_atom (AConst_bool b) ->
    let e' = new_expr_instance ck_substs var_substs expr (Expr_const(Const_bool b)) in
    expand_expr ck_substs var_substs env e'
  | Expr_atom ((AIdent id) as a) ->
    let e' =
    if is_local id env then
      new_expr_instance ck_substs var_substs expr (Expr_ident id)
    else
      new_expr_instance ck_substs var_substs expr (Expr_atom (inline_atom a env))
    in
    expand_expr ck_substs var_substs env e'
  | Expr_const cst ->
      [],[],new_expr_instance ck_substs var_substs expr expr.expr_desc
  | Expr_ident id ->
      let id' = subst_var var_substs id in
      let edesc = Expr_ident id' in
      [],[],new_expr_instance ck_substs var_substs expr edesc
  | Expr_tuple elist ->
      let new_eqs,new_locals,exp_elist =
        expand_list ck_substs var_substs env elist in
      new_eqs, new_locals,
      new_expr_instance ck_substs var_substs expr (Expr_tuple exp_elist)
  | Expr_fby (cst,e) ->
      let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_fby (inline_atom cst env, e') in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_concat (cst,e) ->
      let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_concat (inline_atom cst env, e') in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_tail e ->
      let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_tail e' in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_when (e,c) ->
      let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_when (e',c) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_whennot (e,c) ->
      let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_whennot (e',c) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_merge (c,e1,e2) ->
      let new_eqs1,new_locals1, e1' = expand_expr ck_substs var_substs env e1 in
      let new_eqs2,new_locals2, e2' = expand_expr ck_substs var_substs env e2 in
      let edesc = Expr_merge (c,e1',e2') in
      new_eqs1@new_eqs2,
      new_locals1@new_locals2,
      new_expr_instance ck_substs var_substs expr edesc
  | Expr_appl (id, e) ->
      let decl = Iterenv.get env id in
      begin
        match decl.top_decl_desc with
        | ImportedNode _ ->
            let new_eqs,new_locals, e' = expand_expr ck_substs var_substs env e in
            let edesc = Expr_appl (id, e') in
            new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
        | Node snd ->
            let env' = Iterenv.rollback_until env snd.node_id in
            let new_eqs, new_locals, outs =
              expand_nodeinst ck_substs var_substs env env' e in
            new_eqs, new_locals, new_expr_instance ck_substs var_substs expr outs
        | SensorDecl _ | ActuatorDecl _ | ConstDecl _ -> failwith "Internal error expand_expr"
      end
  | Expr_uclock (e,k) ->
      let new_eqs, new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_uclock (e',k) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_dclock (e,k) ->
      let new_eqs, new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_dclock (e',k) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_phclock (e,q) ->
      let new_eqs, new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_phclock (e',q) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_rate (e,pck) ->
      let new_eqs, new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_rate (e',pck) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_wcetlt (id,e,n) ->
      let new_eqs, new_locals, e' = expand_expr ck_substs var_substs env e in
      let edesc = Expr_wcetlt (id,e',n) in
      new_eqs, new_locals, new_expr_instance ck_substs var_substs expr edesc
  | Expr_map (id,n,e) -> failwith "balh" (* TODO *)

(** Expands an equation *)
and expand_eq ck_substs var_substs env eq =
  let new_eqs, new_locals, expr = expand_expr ck_substs var_substs env eq.eq_rhs in
  let lhs' = List.map (subst_var var_substs) eq.eq_lhs in
  let eq' = {eq_lhs = lhs'; eq_rhs = expr; eq_loc = eq.eq_loc} in
  new_eqs, new_locals, eq'

(** Expands a set of equations *)
and expand_eqs ck_substs var_substs env eqs =
  List.fold_left
    (fun (acc_eqs,acc_locals) eq ->
      let new_eqs, new_locals, eq' = expand_eq ck_substs var_substs env eq in
      (eq'::(new_eqs@acc_eqs)), (new_locals@acc_locals))
    ([],[]) eqs

(** Expands the body of a node, replacing recursively all the node calls
    it contains by the body of the corresponding node. *)
let expand_node env nd =
  let new_eqs, new_locals =
    expand_eqs (Hashtbl.create 10) (Hashtbl.create 10) env nd.node_eqs in
  {node_id = nd.node_id;
   node_type = nd.node_type;
   node_clock = nd.node_clock;
   node_inputs = nd.node_inputs;
   node_outputs = nd.node_outputs;
   node_locals = new_locals@nd.node_locals;
   node_eqs = new_eqs}

let expand_program prog =
  if !Options.main_node = "" then
    raise (Corelang.Error No_main_specified);
  let env =
    try Iterenv.skip_until prog !Options.main_node
    with Not_found -> raise (Corelang.Error Main_not_found)
  in
  let main = Iterenv.next env
  in
  match main.top_decl_desc with
  | ImportedNode _ | SensorDecl _ | ActuatorDecl _ | ConstDecl _ ->
      raise (Corelang.Error Main_wrong_kind)
  | Node nd ->
      expand_node env nd
