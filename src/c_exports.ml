(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate the public functions and structures that enable the access
to the task set in other C files. *)

open C_utils
open Corelang
open Task_set
open Task_graph
open Format

(** Generates a description of the external buffer for communication
    [vref_from->vref_to]. See lib/prelude/extern_com.h for a description of
    the generated structure. *)
let pp_extern_buffer out_f task ty read_proto vref_from vref_to =
  let buf_size = read_proto.rbuf_size in
  let ty = tystring_of_type ty in
  fprintf out_f "@ { /* %s */ sizeof(%s), %d, "
          (combuffer_id (combuffer_name vref_from vref_to)) ty buf_size;
  (match read_proto.rbuf_init with
  | Some cst -> fprintf out_f "%s, " (combuffer_init vref_from vref_to)
  | None -> fprintf out_f "NULL, ");
  fprintf out_f "NULL },"

(** Generates a description of the external buffer for each input of [task]. *)
let pp_extern_buffers_task out_f task =
  List.iter
    (fun (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      pp_extern_buffer out_f task vin.var_type read_proto vref_from vref_to)
    task.task_inputs

(** Generates a description of each external buffer of the [task_set]
    and a function to access them. *)
let pp_extern_buffers out_f task_set =
  (* Generate all buffer descriptions. *)
  fprintf out_f "@[<v 2>static struct %s %s[%s] = {"
    buffer_struct_name static_buffer_set_name nb_buffers_name;
  Hashtbl.iter (fun _ task -> pp_extern_buffers_task out_f task) task_set;
  fprintf out_f "@]@ };@ @ ";

  (* Generate a function to access these descriptions *)
  fprintf out_f "void get_buffer_set (int* buffer_number, struct %s** buffer_set)@ "
    buffer_struct_name;
  fprintf out_f "@[<v 2>{@ ";

  fprintf out_f "*buffer_number = %s;@ " nb_buffers_name;
  fprintf out_f "*buffer_set=%s;" static_buffer_set_name;

  fprintf out_f "@]@ }"

(** Declares the deadline word of [task]. See lib/prelude/dword.h for a description of
    the generated structure. *)
let pp_task_dword_arrays out_f task =
  match task.task_kind with
  | CstTask -> () (* No communications for a constant *)
  | _ ->
      let (pref,pat) = task.task_deadline in
      let dw_pref_length = Array.length pref in
      let dw_pat_length = Array.length pat in
      let tname = task_name task.task_id in
      if dw_pref_length <> 0 then (* Declare prefix *)
        begin
          fprintf out_f "static int %s[%i] = "
            (dword_pref_name tname) dw_pref_length;
          Print.array_printer_from_printer "{@ " ",@ " "@ };"
            (fun out_f (_,v) -> fprintf out_f "%d" v)
            out_f pref
        end;
      if dw_pat_length <> 0 then (* Declare pattern *)
        begin
          fprintf out_f "static int %s[%i] = "
            (dword_pat_name tname) dw_pat_length;
          Print.array_printer_from_printer "{@ " ",@ " "@ };"
            (fun out_f (_,v) -> fprintf out_f "%d" v)
            out_f pat
        end

(** Generates a reference to the deadline word of [task].*)
let pp_dword out_f task =
  let (pref,pat) = task.task_deadline in
  let dw_pref_length = Array.length pref in
  let dw_pat_length = Array.length pat in
  let tname = task_name task.task_id in

  fprintf out_f "{@ ";
  if dw_pref_length = 0 then
    fprintf out_f "NULL,@ "
  else
    fprintf out_f "%s,@ " (dword_pref_name tname);
  fprintf out_f "%i,@ " dw_pref_length;
  if dw_pat_length = 0 then
    fprintf out_f "NULL,@ "
  else
    fprintf out_f "%s,@ " (dword_pat_name tname);
  fprintf out_f "%i@ }" dw_pat_length

(** Generates the deadline of [task]. *)
let pp_deadline out_f task =
  if !Options.no_encoding (* No deadline word *)
  then fprintf out_f "%d" (Deadlines.nth_ud task.task_deadline 0)
  else pp_dword out_f task

(** Generates the parameters of [task], i.e. real-time attributes and C
    functions. See lib/prelude/encoded_task_params.h or
    nonencoded_task_params for a description of the generated
    structures. *)
let pp_task_params out_f task =
  fprintf out_f "{@ \"%s\",@ %i,@ %i,@ %i,@ %a,@ "
    (task_name task.task_id)
    task.task_period task.task_release task.task_wcet (* Real-time attributes *)
    pp_deadline task;
  (* C functions *)
  if !Options.aer_format then
    begin
      if task.task_kind = Sensor then
        fprintf out_f "NULL, "
      else
        fprintf out_f "%s, " (fun_name_A task.task_id);
      fprintf out_f "%s, "  (fun_name_E task.task_id);
      if task.task_kind = Actuator then
        fprintf out_f "NULL, "
      else
        fprintf out_f "%s, " (fun_name_R task.task_id);
      let needs_idx, needs_sidx = needs_idxs task in
      if task.task_kind <> CstTask && (needs_idx || needs_sidx) then
        fprintf out_f "%s" (fun_name_init task.task_id)
      else
        fprintf out_f "NULL";
      fprintf out_f "}"
    end
  else
    fprintf out_f "%s@ }" (fun_name task.task_id)

(** Generates a description of the [task_set] and a function to access it *)
let pp_tasks_params out_f task_set =
  let tasks = Hashtbl.fold
      (fun _ task ts -> if task.task_kind <> CstTask then task::ts else ts)
      task_set []
  in
  if (not !Options.no_encoding) then
    Print.list_printer_from_printer "" "@ " "@ @ "
      (fun out_f t -> pp_task_dword_arrays out_f t)
      out_f tasks;

  (* Define the number of tasks*)
  let nb_tasks = List.length tasks in
  fprintf out_f "#define %s %i@\n" nb_tasks_name nb_tasks;

  (* Declare task parameters. *)
  fprintf out_f "@[<v 2>static struct %s %s[%s] = "
    (task_struct_name ()) static_task_set_name nb_tasks_name;
  Print.list_printer_from_printer "{@ " ",@ " "@]@ };@ @ "
    (fun out_f t -> if t.task_kind <> CstTask then pp_task_params out_f t)
    out_f tasks;

  (* Generate a function to access these parameters *)
  fprintf out_f "void get_task_set (int* task_number, struct %s** task_set)@ "
    (task_struct_name ());
  fprintf out_f "@[<v 2>{@ ";

  fprintf out_f "*task_number = %s;@ " nb_tasks_name;
  fprintf out_f "*task_set=%s;" static_task_set_name;

  fprintf out_f "@]@ }@ @ "

(** Print a pair of job numbers. *)
let pp_job_pair out_f (n,n') =
  fprintf out_f "{%d,%d}" n n'

(** Generates precedence pattern and prefix for [src->dst]. See
    lib/prelude/dword.h for a description of the generated structure.*)
let pp_prec_arrays out_f (src,annot,dst) =
  let (pref,pat) = Precedence_functions.prec_relation annot in
  let pref_length = Hashtbl.length pref in
  let pat_length = Hashtbl.length pat in
  if pref_length <> 0 then (* Prefix *)
    begin
      fprintf out_f "static struct %s %s[%i] = "
        job_prec_struct_name (prec_pref_name (src,dst)) pref_length;
      Print.hashtbl_printer_from_printer "{@ " ",@ " "@ };"
        pp_job_pair out_f pref
    end;
    if pat_length <> 0 then (* Pattern *)
    begin
      fprintf out_f "static struct %s %s[%i] = "
        job_prec_struct_name (prec_pat_name (src,dst)) pat_length;
      Print.hashtbl_printer_from_printer "{@ " ",@ " "@ };"
        pp_job_pair out_f pat
    end

(** Generates a reference to the prefix of precedence [src->dst]. *)
let pp_prec_pref_ptr out_f size src dst =
  if size = 0 then
    fprintf out_f "NULL"
  else
    fprintf out_f "%s" (prec_pref_name (src,dst))

(** Generates a reference to the pattern of precedence [src->dst]. *)
let pp_prec_pat_ptr out_f size src dst =
  if size = 0 then
    fprintf out_f "NULL"
  else
    fprintf out_f "%s" (prec_pat_name (src,dst))

(** Generates a struct describing multi-rate precedence [src->dst]. *)
let pp_precedence out_f src annot dst =
  let (pref,pat) = Precedence_functions.prec_relation annot in
  let pref_length = Hashtbl.length pref in
  let pat_length = Hashtbl.length pat in
  fprintf out_f "{ \"%s\",@ \"%s\",@ %d,@ %d,@ "
    (task_name_of_vertex src) (task_name_of_vertex dst)
    pref_length pat_length;
  pp_prec_pref_ptr out_f pref_length src dst;
  fprintf out_f ",@ ";
  pp_prec_pat_ptr out_f pat_length src dst;
  let hyperperiod = Task_graph.hyperperiod src dst in
  let src_period = hyperperiod/(Task_graph.get_period src) in
  let dst_period = hyperperiod/(Task_graph.get_period dst) in
  fprintf out_f ",@ %d,@ %d@ }"
    src_period dst_period

let pp_precedences_data out_f precs =
  let nb_precs = List.length precs in
  Print.list_printer_from_printer "" "@ " "@ @ "
    (fun out_f prec -> pp_prec_arrays out_f prec)
    out_f precs;
  fprintf out_f "#define %s %i@\n" nb_precs_name nb_precs

(** Generates all precedence descriptions for [task_graph] and a function to access them. *)
let pp_precedences_flat out_f task_graph =
  let precs = prec_list task_graph in
  fprintf out_f "@[<v 2>static struct %s %s[%s] = "
    prec_struct_name static_prec_set_name nb_precs_name;
  Print.list_printer_from_printer "{@ " ",@ " "@]@ };@ @ "
    (fun out_f (src,annot,dst) -> pp_precedence out_f src annot dst)
    out_f precs;

  (* Function to access precedences *)
  fprintf out_f "void get_precedence_set (int* prec_number, struct %s** prec_set)@ "
    prec_struct_name;
  fprintf out_f "@[<v 2>{@ ";

  fprintf out_f "*prec_number = %s;@ " nb_precs_name;
  fprintf out_f "*prec_set=%s;" static_prec_set_name;

  fprintf out_f "@]@ }@ @ "

let pp_precedence_sorted out_f task src_precs dst_precs =
  fprintf out_f "@[<v2>{ @[<h>\"%s\",@ %d,@ %d,@]@ "
    (C_utils.task_name task.task_id)
    (List.length src_precs)
    (List.length dst_precs);
  if List.length src_precs > 0 then
    Print.list_printer_from_printer
      "@[<v 2>(struct multirate_precedence[]){@ " ",@ " "@]}"
      (fun out_f (src,annot,dst) -> pp_precedence out_f src annot dst)
      out_f src_precs
  else
    fprintf out_f "NULL";
  fprintf out_f ",@ ";
  if List.length dst_precs > 0 then
    Print.list_printer_from_printer
      "@[<v 2>(struct multirate_precedence[]){@ " ",@ " "@]}"
      (fun out_f (src,annot,dst) -> pp_precedence out_f src annot dst)
      out_f dst_precs
  else
    fprintf out_f "NULL";
  fprintf out_f "@] }"

let pp_precedences_sorted out_f task_graph task_set =
  let precs = prec_list task_graph in
  fprintf out_f "@[<v 2>static struct %s %s[%s] = "
    "task_precedences" static_prec_set_name nb_tasks_name;
  let tasks =
    Hashtbl.fold
      (fun _ task acc ->
         let src_precs =
           List.filter
             (fun (_,_,dst) -> (taskid_of_vertex dst) = task.task_id)
             precs in
         let dst_precs =
           List.filter
             (fun (src,_,_) -> (taskid_of_vertex src) = task.task_id)
             precs in
         ((task),(src_precs),(dst_precs))::acc)
      task_set
      []
  in
  Print.list_printer_from_printer "{@ " ",@ " "@]@ };@ @ "
    (fun out_f (task,src_precs,dst_precs) -> pp_precedence_sorted out_f task src_precs dst_precs)
    out_f tasks;

  (* Function to access precedences *)
  fprintf out_f "void get_sorted_precedence_set (int* prec_number, struct %s** prec_set)@ "
    "task_precedences";
  fprintf out_f "@[<v 2>{@ ";

  fprintf out_f "*prec_number = %s;@ " nb_precs_name;
  fprintf out_f "*prec_set=%s;" static_prec_set_name;

  fprintf out_f "@]@ }@ @ "

let pp_precedences out_f task_graph task_set =
  pp_precedences_data out_f (prec_list task_graph);
  if !Options.flat_precedence_set then
    pp_precedences_flat out_f task_graph;
  if !Options.sorted_precedence_set then
    pp_precedences_sorted out_f task_graph task_set

(** Generates a description of communication
    [tpred_name.vpred_id->tsucc_name.vsucc_id]. See
    lib/prelude/com_patterns.h for a description of the generated structure. *)
let pp_protocol_export out_f ((tpred_name,tpred_tag),vpred_id,write_proto,
                              (tsucc_name,tsucc_tag),vsucc_id,read_proto) =
  let read_name =
    if read_proto.rbuf_size > 1 && (uses_read_protocol read_proto) then
      "&"^(read_proto_name (tsucc_name,tsucc_tag) vsucc_id)
    else "NULL"
  in
  let write_name =
    if write_proto.wbuf_size > 1 && (uses_write_protocol write_proto) then
      "&"^(write_proto_name_bis (tpred_name,tpred_tag) vpred_id
             (tsucc_name,tsucc_tag) vsucc_id)
    else "NULL"
  in
  let buff_size = read_proto.rbuf_size in
  let tpred_string =
      "\""^(task_name (tpred_name,tpred_tag))^"\""
  in
  let tsucc_string =
      "\""^task_name (tsucc_name,tsucc_tag)^"\""
  in
  fprintf out_f "{ %s,@ \"%s\",@ %s,@ \"%s\",@ %s,@ %s,@ %d@ }"
    tpred_string vpred_id tsucc_string vsucc_id write_name read_name buff_size

(** Generates a description for each communication of the [task_set] and
    a function to access these descriptions. *)
let pp_protocols_export out_f task_set =
  let coms = coms_list task_set in
  let nb_coms = List.length coms in
  (* Number of coms*)
  fprintf out_f "#define %s %i@\n" nb_coms_name nb_coms;

  (* Coms description *)
  fprintf out_f "@[<v 2>static struct %s %s[%s] = "
    com_struct_name static_com_set_name nb_coms_name;
  Print.list_printer_from_printer "{@ " ",@ " "@]@ };@ @ "
    pp_protocol_export out_f coms;

  (* Function to acess coms *)
  fprintf out_f "void get_communication_set (int* com_number, struct %s** com_set)@ "
    com_struct_name;
  fprintf out_f "@[<v 2>{@ ";

  fprintf out_f "*com_number = %s;@ " nb_coms_name;
  fprintf out_f "*com_set=%s;" static_com_set_name;

  fprintf out_f "@]@ }@ @ "
