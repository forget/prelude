(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)
open Format
open C_node_skeletons
  
let usage = "Usage: prelude_genwrapper [options] <source-file>"

let extension = ".plu"

let load_lusi lusi_filename =
  begin
    if !lusi_filename = "" then
      printf "lusi file unknown\n"
    else
      printf "Will use lusi file: %s\n" !lusi_filename;
  end

let compile basename =
  let source_name = basename^extension in
  Location.input_name := source_name;
  let lexbuf = Lexing.from_channel (open_in source_name) in
  Location.init lexbuf source_name;
  (* Parsing *)
  let prog_list, prog_assoc =
    try
      Parse.prog lexbuf
    with (Lexer.Error loc) | (Parse.Syntax_err loc) as exc ->
        Parse.report_error loc;
        raise exc
  in
  (* Typing *)
  begin
    try
      Typing.type_prog Type_predef.env prog_list
    with (Types.Error (loc,err)) as exc ->
      Location.print loc;
      Types.pp_error err;
      raise exc
  end;
  (* Clock calculus *)
  begin
    try
      Clock_calculus.clock_prog Clock_predef.env prog_list
    with (Clocks.Error (loc,err)) as exc ->
      Location.print loc;
      Clocks.pp_error err;
      raise exc
  end;
  let in_list = Corelang.get_IN_list prog_list in
  (* Produce imported node header if requested *)
  if Gwoptions.gen_IN_header() then
    C_node_skeletons.pp_imported_node_header in_list basename;
  (* Produce imported node body  if requested
   * This done for either plain C template
   * or for lustrec compiler C wrapper.
   *)
  if Gwoptions.gen_IN_body() then
    if Gwoptions.gen_IN_forLustrec() then
      C_node_skeletons.pp_imported_node_lustrec_body in_list basename
    else
      C_node_skeletons.pp_imported_node_body in_list basename;

  (* Flatten nodes hierarchy*)
  let exp_main =
    try
      Expand.expand_program prog_assoc
    with (Corelang.Error err) as exc ->
      Corelang.pp_error err;
      raise exc
  in
  (* Causality analysis *)
  begin
    try
      Causality_analysis.check_causal_prog prog_list
    with (Causality_analysis.Cycle v) as exc ->
      Causality_analysis.pp_error v;
      raise exc
  end;
  (* Extract task graph *)
  let gred =
    try
      Task_graph.reduced_task_graph exp_main prog_assoc
    with (Task_graph.Error (loc,err)) as exc ->
      Location.print loc;
      Task_graph.pp_error err;
      raise exc
  in
  (* Transform into task set *)
  let task_set =
    try
      Task_set_construction.of_task_graph gred exp_main prog_assoc
    with (Task_set.Error (loc,err)) as exc ->
      Location.print loc;
      Task_set.pp_error err;
      raise exc
  in
  (* Produce sensor/actuator code *)
  if Gwoptions.gen_IN_header() && !Options.io_code then
    C_node_skeletons.pp_sensor_actuator_header task_set basename;
  if Gwoptions.gen_IN_body() && !Options.io_code then
    C_node_skeletons.pp_sensor_actuator_body task_set basename

let anonymous filename =
  if Filename.check_suffix filename extension
  then
    let basename = Filename.chop_suffix filename extension in
    begin
      if Gwoptions.gen_IN_forLustrec() then
	load_lusi(Gwoptions.lusi_filename);
      compile basename
    end
  else
    raise (Arg.Bad ("Can only compile *.plu files"))

let _ =
  try
    Arg.parse Gwoptions.options anonymous usage
  with
  | Parse.Syntax_err _ | Lexer.Error _ | Types.Error (_,_) | Clocks.Error (_,_)
  | Corelang.Error _ | Task_set.Error _ | Causality_analysis.Cycle _
  | Task_graph.Error _ -> ()
  | Corelang.Unbound_type (ty,loc) ->
    Location.print loc;
    fprintf err_formatter "@[Unbound type %s@." ty
  | exc -> raise exc
