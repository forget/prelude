(* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- *)

(** Generate C node functions: step functions, A/E/R functions, init functions. *)

open Corelang  
open Task_set  
open C_utils
open Format  

let collapse_array array =
  let (last_v,cpt,l) =
    Array.fold_left
      (fun (prec_v,cpt,l) v ->
        match prec_v with
        | None -> Some v, 1, l
        | Some pv ->
            if pv = v then
              Some v, cpt+1, l
            else
              Some v, 1, (pv,cpt)::l
      )
      (None,0,[]) array
  in
  match last_v with
  | None ->  List.rev l
  | Some lv -> List.rev ((lv,cpt)::l)

let pp_array_slice printer out_f (v,repeat) =
  if repeat < array_collapse_threshold then
    begin
      for i=0 to repeat - 1 do
        printer out_f init_idx v;
        fprintf out_f "@ %s++;@ " init_idx
      done
    end
  else
    begin
      fprintf out_f "@[<v 2>for(int %s=%s; %s<%s+%d; %s++) {@ " init_sidx init_idx
        init_sidx init_idx repeat init_sidx;
      printer out_f init_sidx v;
      fprintf out_f "@]@ }@ %s=%s-1;@ " init_idx init_sidx
    end
  
let pp_init_array out_f printer array =
  let col_array = collapse_array array in
  fprintf out_f "%s=0;@ " init_idx;
  Print.list_printer_from_printer "" "@ " "@ "
    (fun out_f (v,repeat) -> pp_array_slice printer out_f (v,repeat)) out_f col_array

(** Generates the initialization function for the buffer of
    communication [vref_from->vref_to]. For multi-core architectures. *)
let pp_buffer_init_fun out_f task ty read_proto vref_from vref_to =
  let bname = combuffer_name vref_from vref_to in
  let funname = combuffer_init vref_from vref_to in
  let ty = tystring_of_type ty in
  let buf_size = read_proto.rbuf_size in
  if buf_size > 1 then
    begin
      match read_proto.rbuf_init with
      | Some cst ->
          fprintf out_f "void %s()@ {@[<v 2>@ " funname;
          fprintf out_f "int %s;@ " init_idx;
          if buf_size >= array_collapse_threshold then
            fprintf out_f "int %s;@ " init_sidx;
          fprintf out_f "%s %s[%i];@ " ty bname buf_size;
          pp_init_array out_f
            (fun out_f idx_name c -> fprintf out_f "%s[%s]=%s;" bname idx_name
                (string_of_atom c))
            (Array.make buf_size cst);
          fprintf out_f "@[<v 2>for (%s = 0; %s < %d; %s++) {@ "
            init_idx init_idx buf_size init_idx;
          let buf_id = combuffer_id (combuffer_name vref_from vref_to) in
          fprintf out_f "write_val(%s, %s, sizeof(%s[%s]), &%s[%s]);"
            buf_id init_idx bname init_idx bname init_idx;
          fprintf out_f "@]@ }";
          fprintf out_f "@]@ }@ "
      | None -> () (* Don't care about initial values *)
    end
  else
    begin
      match read_proto.rbuf_init with
      | Some cst -> failwith "Buffer initialization not supported" (* ?? *)
      | None -> () (* Don't care about initial values *)
    end

(** Generates initialization functions for all buffers of [task] *)      
let pp_buffer_init_funs_task out_f task =
  List.iter
    (fun (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      pp_buffer_init_fun out_f task vin.var_type read_proto vref_from vref_to)
    task.task_inputs

let pp_buffer_init_fun_call out_f task ty read_proto vref_from vref_to =
  let funname = combuffer_init vref_from vref_to in
  let buf_size = read_proto.rbuf_size in
  if buf_size > 1 then
    begin
      match read_proto.rbuf_init with
      | Some cst ->
          fprintf out_f "%s();@ " funname
      | None -> ()
    end
    
(** Generates initialization functions for all buffers of [task] *)      
let pp_buffer_init_fun_calls_task out_f task =
  List.iter
    (fun (vin,vref_from,read_proto) ->
      let vref_to = vref_of_task_var task vin in
      pp_buffer_init_fun_call out_f task vin.var_type read_proto vref_from vref_to)
    task.task_inputs

    
(** Generates initialization functions for all buffers of the [task_set] *)   
let pp_buffer_init_funs out_f task_set =
  Hashtbl.iter (fun _ task -> pp_buffer_init_funs_task out_f task) task_set;
  fprintf out_f "@ "

(** Produce the read communication protocol initialization function for
    communication [vref_pred->vin] *)
let pp_read_proto_init_fun out_f tid (vin,vref_pred,proto) =
    if proto.rbuf_size > 1 then
    let pref_length, pat_length =
      (Array.length proto.change_pref),
      (Array.length proto.change_pat) in
    if (pref_length > 1) || (pat_length > 1) then
      begin
        let proto_name = read_proto_name tid vin.var_id in
        (* Prefix *)
        if pref_length > 0 then
          begin
            pp_init_array out_f
              (fun out_f idx_name v -> fprintf out_f "%s.%s[%s]=%s;"
                  proto_name read_proto_pref_name idx_name (cstring_of_bool v))
              proto.change_pref;

          end
        else
          fprintf out_f "%s.%s=NULL;@ " proto_name read_proto_pref_name;
        fprintf out_f "%s.%s=%i;@ " proto_name read_proto_pref_size_name pref_length;
        (* Pattern *)
        if pat_length > 0 then
          begin
            pp_init_array out_f
              (fun out_f idx_name v -> fprintf out_f "%s.%s[%s]=%s;"
                  proto_name read_proto_pat_name idx_name (cstring_of_bool v))
              proto.change_pat;
          end
        else
          fprintf out_f "%s.%s=NULL;@ " proto_name read_proto_pat_name;
        fprintf out_f "%s.%s=%i;@ @ " proto_name read_proto_pat_size_name pat_length;
      end


(** Produce the write communication protocol initialization function for
    communication [vout->vref_succ] *)
let pp_write_proto_init_fun out_f tid vout vref_succ proto =
  if proto.wbuf_size > 1 then
    let pref_length, pat_length =
      (Array.length proto.write_pref),
      (Array.length proto.write_pat) in
    if  (pref_length > 1) || (pat_length > 1) then
      begin
        let proto_name = write_proto_name tid vout.var_id vref_succ in
        (* Prefix *)
        if pref_length > 0 then
          begin
            pp_init_array out_f
              (fun out_f idx_name v -> fprintf out_f "%s.%s[%s]=%s;"
                  proto_name write_proto_pref_name idx_name (cstring_of_bool v))
              proto.write_pref;
          end
        else
          fprintf out_f "%s.%s=NULL;@ " proto_name write_proto_pref_name;
        fprintf out_f "%s.%s=%i;@ " proto_name write_proto_pref_size_name pref_length;
        (* Pattern *)
        if pat_length > 0 then
          begin
            pp_init_array out_f
              (fun out_f idx_name v -> fprintf out_f "%s.%s[%s]=%s;"
                  proto_name write_proto_pat_name idx_name (cstring_of_bool v))
              proto.write_pat;
          end
        else
          fprintf out_f "%s.%s=NULL;@ " proto_name write_proto_pat_name;
        fprintf out_f "%s.%s=%i;@ @ " proto_name write_proto_pat_size_name pat_length;
      end
    
(** Produce the write communication protocol initialization functions
    for all the [readers] of [vout] *)
let pp_write_protos_init_fun out_f tid (vout,readers) =
  List.iter
    (fun (vref_succ,proto) -> pp_write_proto_init_fun out_f tid vout vref_succ proto)
    readers

let pp_proto_array_assign out_f pref_length pat_length pref_name pat_name proto_name buf_size pref_array pat_array  =
  if buf_size > 1 then
    if pref_length > 1 then
      fprintf out_f "%s.%s = %s;@ " proto_name pref_name pref_array;
    if pat_length > 1 then
      fprintf out_f "%s.%s = %s;@ " proto_name pat_name pat_array

let pp_rproto_array_assign out_f tid (vin,vref,proto) =
  let proto_name = read_proto_name tid vin.var_id in
  pp_proto_array_assign
    out_f
    (Array.length proto.change_pref)
    (Array.length proto.change_pat)
    read_proto_pref_name
    read_proto_pat_name
    proto_name
    proto.rbuf_size
    (proto_array_name proto_name true)
    (proto_array_name proto_name false)

let pp_wproto_array_assign out_f tid (vout,readers) = 
  List.iter
    (fun (vref_succ,proto) ->
      let proto_name = write_proto_name tid vout.var_id vref_succ in
      pp_proto_array_assign
        out_f
        (Array.length proto.write_pref)
        (Array.length proto.write_pat)
        write_proto_pref_name
        write_proto_pat_name
        proto_name
        (proto.wbuf_size)
        (proto_array_name proto_name true)
        (proto_array_name proto_name false))

(** Generates communication protocol and buffer initialization functions for the
    inputs and outputs of [task] *)
let pp_init_funs_task out_f task needs_idx needs_sidx =
  pp_buffer_init_funs_task out_f task;

  fprintf out_f "int %s()@ @[<v 2>{@ " (fun_name_init task.task_id);
  if needs_idx then
    fprintf out_f "int %s=0;@ " init_idx;
  if needs_sidx then
    fprintf out_f "int %s=0;@ @ " init_sidx;

  pp_buffer_init_fun_calls_task out_f task;

  List.iter (fun (vin,vref,proto)  ->
    let proto_name = read_proto_name task.task_id vin.var_id in
    pp_proto_array_assign
      out_f
      (Array.length proto.change_pref)
      (Array.length proto.change_pat)
      read_proto_pref_name
      read_proto_pat_name
      proto_name
      (proto.rbuf_size)
      (proto_array_name proto_name true)
      (proto_array_name proto_name false)) task.task_inputs;
  List.iter (fun (vout,readers) ->
    List.iter
      (fun (vref_succ,proto) ->
        let proto_name = write_proto_name task.task_id vout.var_id vref_succ in
        pp_proto_array_assign
          out_f
          (Array.length proto.write_pref)
          (Array.length proto.write_pat)
          write_proto_pref_name
          write_proto_pat_name
          proto_name
          (proto.wbuf_size)
          (proto_array_name proto_name true)
          (proto_array_name proto_name false)) readers) task.task_outputs;
  
  List.iter (pp_read_proto_init_fun out_f task.task_id) task.task_inputs;
  List.iter (pp_write_protos_init_fun out_f task.task_id) task.task_outputs;

  fprintf out_f "@ return 0;";
  fprintf out_f "@]@ }@ @ "
    
    
(** Generates communication protocol and buffer initializations as functions,
    instead of litterals at top-level *)
let pp_init_funs out_f task_set =
  Hashtbl.iter
    (fun _ t -> 
      let needs_idx, needs_sidx = needs_idxs t in
      if (t.task_kind <> CstTask) && (needs_idx || needs_sidx) then
        pp_init_funs_task out_f t needs_idx needs_sidx)
    task_set;
  fprintf out_f "@ "
    
(** Generates access to local input [vin] *)   
let pp_lread_source out_f task vin vref_pred proto =
  match vref_pred with
  | ConstVal cst ->
      fprintf out_f "%s" cst
  | _ ->
      let vref_in = vref_of_task_var task vin in
      let buf_name = if !Options.extern_buffers || !Options.aer_format then
        local_rbuffer_name task.task_id vin.var_id
      else
        combuffer_name vref_pred vref_in
      in
      fprintf out_f "%s" buf_name;
      if proto.rbuf_size > 1 then
        fprintf out_f "[%s]" (read_cell_pointer_name vin);
      if Options.trace_instances () then
        fprintf out_f ".value"

(** Generates update of the read buffer index *)   
let pp_read_cell_update out_f tid (vin,vref_pred,proto) =
  let pointer_name = read_cell_pointer_name vin in
  let proto_name = read_proto_name tid vin.var_id in
  let uses_protocol = uses_read_protocol proto in
  if proto.rbuf_size > 1 then
    begin
      if uses_protocol then
        begin
          fprintf out_f "@[<v 2>if(must_change(%s,%s))@ " proto_name inst_cpt_name
        end;
      fprintf out_f "%s=(%s+1)%%%i;" pointer_name pointer_name proto.rbuf_size;
      if uses_protocol then
        fprintf out_f "@]";
      fprintf out_f "@ "
    end

(** Generates a copy from extern communication variable to local input
    variable [vin]. For multi-core architectures. *)
let pp_lread_copy_extern out_f task (vin,vref_pred,proto) =
  let vref_in = vref_of_task_var task vin in
  let buf_name = local_rbuffer_name task.task_id vin.var_id in

   (* Generate a call to the generic copy function read_val *)
  fprintf out_f "read_val(%s, " (combuffer_id (combuffer_name vref_pred vref_in));
  if proto.rbuf_size > 1 then
    fprintf out_f "%s, " (read_cell_pointer_name vin)
  else
    fprintf out_f "0, ";
  fprintf out_f "sizeof(%s), &%s);@ " buf_name buf_name

(** Generates a copy from communication variable to local input variable [vin].*)   
let pp_lread_copy_local out_f task (vin,vref_pred,proto) =
  let buf_name = local_rbuffer_name task.task_id vin.var_id in

  if (Types.is_array vin.var_type) then
    begin
      fprintf out_f "memcpy(%s," buf_name;
      pp_lread_source out_f task vin vref_pred proto;
      fprintf out_f ",";
      fprintf out_f "sizeof(";
      pp_lread_source out_f task vin vref_pred proto;
      fprintf out_f "));@ "
    end
  else
    begin
      fprintf out_f "%s=" buf_name;
      pp_lread_source out_f task vin vref_pred proto;
      fprintf out_f ";@ "
    end

(** Generate activation conditions for [task]. *)   
let pp_conds out_f task conds =
  Print.list_printer_from_printer "@[<v 2>if(" "&&" ")@ "
    (fun out cond -> 
      if (not cond.cond_val) then fprintf out_f "! ";
      fprintf out_f "%s" (local_rbuffer_name task.task_id cond.cond_var.var_id))
    out_f conds
   
(** Generates a copy from local output variable [src_name] to the
    corresponding communication variable. *)
let pp_lwrite_copy out_f dest_name pointer_name src_name ty =
   if !Options.extern_buffers || !Options.aer_format then
   (* Generate a call to the generic copy function write_val *)
    let idx = match pointer_name with
    | Some ptr -> ptr
    | None -> "0"
    in
    fprintf out_f "write_val(%s, %s, sizeof(%s), &%s);@ "
      (combuffer_id dest_name) idx src_name src_name
  else (* Just copy the variable *)
    let oname = match pointer_name with
    | Some ptr -> dest_name^"["^ptr^"]"
    | None -> dest_name
    in
    match (Types.repr ty).Types.tdesc with
    | Types.Tarray _ ->
        fprintf out_f "memcpy(%s,%s,sizeof(%s));@ " oname src_name src_name
    | _ -> fprintf out_f "%s=%s;@ " oname src_name

(** Generates output update for [vout] based on communication protocol
    [proto]. Also updates protocol indices. *)
let pp_lwrite_update out_f task vout vref_succ proto =
  let pointer_name = write_cell_pointer_name vout vref_succ in
  let proto_name = write_proto_name task.task_id vout.var_id vref_succ in
  let uses_protocol = uses_write_protocol proto in
  if uses_protocol then
    begin
      fprintf out_f "@[<v 2>if(must_write(%s,%s)) {@ " proto_name inst_cpt_name
    end;
  let com_buf = combuffer_name (vref_of_task_var task vout) vref_succ in
  let out_var = outvar_name task vout in
  if Options.trace_instances () then
    if proto.wbuf_size > 1 then
      begin
        pp_lwrite_copy out_f com_buf (Some pointer_name) out_var vout.var_type;
        fprintf out_f "%s[%s].instance=%s;@ " com_buf pointer_name inst_cpt_name;
        fprintf out_f "%s=(%s+1)%%%i;" pointer_name pointer_name proto.wbuf_size
      end
    else
      begin
        pp_lwrite_copy out_f com_buf None out_var vout.var_type;
        fprintf out_f "%s.instance=%s;" com_buf inst_cpt_name
      end
  else
    if proto.wbuf_size > 1 then (* Update write buffer index *)
      begin
        pp_lwrite_copy out_f com_buf (Some pointer_name) out_var vout.var_type;
        fprintf out_f "%s=(%s+1)%%%i;" pointer_name pointer_name proto.wbuf_size
      end
    else
      pp_lwrite_copy out_f com_buf None out_var vout.var_type;
  if uses_protocol then
    fprintf out_f "@]@ }@ "
  else
    fprintf out_f "@ "

(** Generate all output updates for [task] *)   
let pp_lwrite_updates out_f task (vout,readers) =
  List.iter
    (fun (vref_succ,proto) -> pp_lwrite_update out_f task vout vref_succ proto)
    readers

let returns_array task =
  if (List.length task.task_outputs == 1) then
    begin
      let ret_var = fst (List.hd task.task_outputs) in
      Types.is_array ret_var.var_type
    end
  else
    false
    
let pp_return out_f task =
  if ((List.length task.task_outputs == 1) && (not (returns_array task))) then
    let vout,_ = List.hd task.task_outputs in
    let out_name = local_wbuffer_name task.task_id vout.var_id in
    fprintf out_f "%s=" out_name
      
let pp_parameters out_f task =
  fprintf out_f "(";
  (* Print inputs *)
  Print.list_printer_from_printer "" "," ""
    (fun out (vin,vref_pred,proto) ->
      fprintf out_f "%s" (local_rbuffer_name task.task_id vin.var_id))
    out_f task.task_inputs;
  (* Also pass eventual return array as parameter *)
  if (returns_array task) then
    begin
      let ret_array = fst (List.hd task.task_outputs) in
      if (List.length task.task_inputs > 0) then
        fprintf out_f ",";
      fprintf out_f "&%s" (local_wbuffer_name task.task_id ret_array.var_id);
    end;
  if (List.length task.task_outputs > 1) then
    fprintf out_f ",&%s" (outstruct_name task.task_id);
  fprintf out_f ")"

let call_name task =
  match task.task_kind with
  | StdTask -> external_name task.task_id
  | Sensor -> sensor_name task.task_id
  | Actuator -> actuator_name task.task_id
  | WCETTask (id,_) -> wcetlt_name id
  | MergeTask | CstTask -> failwith "call_name"
    
(** Generates a call to the C function that actually implements the
    imported node [task]. *)
let pp_function_call out_f task =
  if task.task_conds <> [] then
    pp_conds out_f task task.task_conds;
  begin
    match task.task_kind with
    | StdTask | Sensor | Actuator ->
        let fun_name = call_name task in
        pp_return out_f task;
        fprintf out_f "%s" fun_name;
        pp_parameters out_f task;
        fprintf out_f ";@ "
    | WCETTask (id,n)->
        let fun_name = call_name task in
        let vout,_ = List.hd task.task_outputs in
        fprintf out_f "%s=%s" vout.var_id fun_name;
        Print.list_printer_from_printer "(" "," ""
              (fun out (vin,vref_pred,proto) ->
                fprintf out_f "%s" (local_rbuffer_name task.task_id vin.var_id))
              out_f task.task_inputs;
        fprintf out_f ",%d);@ " n
    | MergeTask ->
        let (var_c,c_pred,c_proto),(var_i1,i1_pred,i1_proto),(var_i2,i2_pred,i2_proto) =
          match task.task_inputs with
          | [c;i1;i2] -> (c,i1,i2)
          | _ -> failwith "Internal error: pp_function_call"
        in
        let vout,_ = List.hd task.task_outputs in
        fprintf out_f "@[<v 2>if(%s)@ " (local_rbuffer_name task.task_id var_c.var_id);
        let out_name = local_wbuffer_name task.task_id vout.var_id in
        
        fprintf out_f "%s=%s;@]@ " out_name
          (local_rbuffer_name task.task_id var_i1.var_id);

        fprintf out_f "@[<v 2>else@ %s=%s;@]@ " out_name
          (local_rbuffer_name task.task_id var_i2.var_id)
    | CstTask -> () (* Printed directly when printing input of consuming node *)
  end;
  if task.task_conds <> [] then
    fprintf out_f "@]@ "

(** Generates the signature of the C function for [task]. *)
let pp_task_header out_f task =
  fprintf out_f "int %s(void* args)@ "(fun_name task.task_id);
  fprintf out_f "@[<v 2>{@ "

(** Declares the index of the read protocol for input [vin]. *)    
let pp_read_idx out_f (vin,_,proto) =
  if proto.rbuf_size > 1 then
    fprintf out_f "static int %s=0;@ " (read_cell_pointer_name vin)

(** Declares the index of the write protocol of each reader of output [vout]. *)   
let pp_write_idx out_f (vout,readers) =
  List.iter
    (fun (vref_succ,proto) ->
      if proto.wbuf_size > 1 then
        let init_cell = match proto.wbuf_init with | Some _ -> 1 | None -> 0 in
        fprintf out_f "static int %s=%i;@ "
          (write_cell_pointer_name vout vref_succ)
          init_cell)
    readers

(** Generates the C function of [task] *)   
let pp_task_body out_f types_f task =
  pp_task_header out_f task;

  (* Variables *)
(*      List.iter (pp_delay_init out_f) precs; *) (* TO DO *)
  C_buffers.pp_lread_allocs_task out_f task false;
  C_buffers.pp_lwrite_allocs_task out_f types_f task false;
  List.iter (pp_read_idx out_f) task.task_inputs;
  List.iter (pp_write_idx out_f) task.task_outputs;
  (* if any task uses protocol in ntask.task_inputs @ task.task_outputs *)
  let needs_instance_var = any_uses_read_proto task.task_inputs || any_uses_write_proto task.task_outputs true in
  if needs_instance_var then
    fprintf out_f "static int %s=0;@ @ " inst_cpt_name;
  C_traces.pp_trace_buf_alloc out_f task;

   (* Copy communication variables to local inputs *)
  if !Options.extern_buffers then
    List.iter (pp_lread_copy_extern out_f task) task.task_inputs
  else
    List.iter (pp_lread_copy_local out_f task) task.task_inputs;

  fprintf out_f "@ ";
  
  List.iter (pp_read_cell_update out_f task.task_id) task.task_inputs;
  fprintf out_f "@ ";
  
  (* Function call *)
  pp_function_call out_f task;

  fprintf out_f "@ ";
  
  (* Insert code for tracing *)
  C_traces.pp_trace_node out_f task;

  (* Update outputs *)
  List.iter (pp_lwrite_updates out_f task) task.task_outputs;

   if needs_instance_var then
    fprintf out_f "@ %s++;@ @ " inst_cpt_name;
  fprintf out_f "return 0;";

  fprintf out_f "@]@ }@ " (* close function *)

(** Generates the C function of each task of [task_set]. Non-AER mode. *)   
let pp_task_bodies out_f types_f task_set =
  Hashtbl.iter
    (fun _ t -> if t.task_kind <> CstTask then 
      begin pp_task_body out_f types_f t; fprintf out_f "@ " end)
   task_set
   
(** Generates acquisition function of [task] *)   
let pp_task_A out_f task =
  fprintf out_f "int %s(void* args)@ " (fun_name_A task.task_id);
  fprintf out_f "@[<v 2>{@ ";

  let uses_protocol = any_uses_read_proto task.task_inputs in
  List.iter (pp_read_idx out_f) task.task_inputs;
  if uses_protocol then
    fprintf out_f "static int %s=0;@ @ " inst_cpt_name;
  List.iter (pp_lread_copy_extern out_f task) task.task_inputs;
  List.iter (pp_read_cell_update out_f task.task_id) task.task_inputs;
  if uses_protocol then
    fprintf out_f "%s++;@ @ " inst_cpt_name;

  fprintf out_f "return 0;";
  fprintf out_f "@]@ }@ "

(** Generates execute function of [task] *)   
let pp_task_E out_f task =
  fprintf out_f "int %s(void* args)@ " (fun_name_E task.task_id);
  fprintf out_f "@[<v 2>{@ ";

  pp_function_call out_f task;

  fprintf out_f "return 0;";
  fprintf out_f "@]@ }@ "

(** Generates restitution function of [task] *)   
let pp_task_R out_f task =
  fprintf out_f "int %s(void* args)@ " (fun_name_R task.task_id);
  fprintf out_f "@[<v 2>{@ ";

  let uses_protocol = any_uses_write_proto task.task_outputs true in
  List.iter (pp_write_idx out_f) task.task_outputs;
  if uses_protocol then
    fprintf out_f "static int %s=0;@ @ " inst_cpt_name;
  List.iter (pp_lwrite_updates out_f task) task.task_outputs;
  if uses_protocol then
    fprintf out_f "%s++;@ @ " inst_cpt_name;

  fprintf out_f "return 0;";
  fprintf out_f "@]@ }@ "

(** Generates A/E/R functions for each task of [task_set]. *)   
let pp_task_AERs out_f task_set =
  Hashtbl.iter
    (fun _ t -> match t.task_kind with
      Sensor ->
        pp_task_E out_f t;
        fprintf out_f "@ ";
        pp_task_R out_f t;
        fprintf out_f "@ "
    |  Actuator ->
        pp_task_A out_f t;
        fprintf out_f "@ ";
        pp_task_E out_f t;
        fprintf out_f "@ "
    | CstTask -> ()
    | _ ->
        pp_task_A out_f t;
        fprintf out_f "@ ";
        pp_task_E out_f t;
        fprintf out_f "@ ";
        pp_task_R out_f t;
        fprintf out_f "@ "
    )
    task_set
    
