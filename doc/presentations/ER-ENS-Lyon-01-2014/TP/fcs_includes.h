#ifndef _fcs_includes_H
#define _fcs_includes_H
#include <stdio.h>
#include <string.h>
#include "fcs_c/fcs.h"

/* This file is user-defined, not generated */

int NL(int pos_c, int pos_o);
int NF(int pos_i);
int PL(int acc_c, int acc_o);
int PF(int acc_i);
int FL(int angle_c, int angle_o);
int FF(int angle);
void AP(int acc, int position, struct AP_outs_t* outs);
int input_pos_c(void);
int input_angle(void);
int input_position(void);
int input_acc(void);
void output_order(int i);

#endif
