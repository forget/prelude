\documentclass{article}
\usepackage{TP}
\usepackage{xspace}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{dcolumn}
\usepackage{ifthen}
\graphicspath{{figures/}}

\lstset{basicstyle=\ttfamily}
\input{lstdefine-CMake}
\input{lstdefine-SchedMCore-taskset}
\input{lstdefine-Prelude}

\newcommand{\prelude}{{\sc Prelude}\xspace}
\newcommand{\schedmcore}{{\sc SchedMCore}\xspace}
\newcommand{\runner}{{\sc SchedMCore Runner}\xspace}
\newcommand{\converter}{{\sc SchedMCore Converter}\xspace}
\newcommand{\uppaal}{{\sc Uppaal}\xspace}
\newcommand{\todo}[1]{{\color{red}TODO: #1}}
\newcommand{\code}[1]{\lstinline!#1!}
\newcommand{\lttng}{LTTng\xspace}

\newtheorem{exercise}{Exercise}

\author{Julien Forget\footnote{Based on a practical session by Wolfgang Puffitsch, Julien Forget, Eric Noulard and Claire Pagetti}}
\ecole{ENS Lyon}

\newtheorem{exercice}{Exercise}


\sujetTP{Practical session:\\ \prelude and \schedmcore}
\enseignement{Real-time in the Synchronous approach}
\shortenseignement{\prelude \& \schedmcore}

\date{January 17, 2014}


\begin{document}
\newboolean{reponse}
%\setboolean{reponse}{true}
\setboolean{reponse}{false}

\sloppy

\maketitle

The goal of this practical session is to illustrate how real-time
constraints can be handled in a synchronous language context.

\section{Basics of \prelude}
The first exercise is based on the \code{sampling_loop} example of
\prelude\ distribution. Its structure is recalled in
Figure~\ref{fig:sampling}.

\begin{figure}[hbt]
  \centering
  \scalebox{.8}{\input{figures/sampling.pspdftex}}
  \caption{Sampling loop}
  \label{fig:sampling}
\end{figure}

\subsection{Build with \prelude compiler}
\begin{itemize}
\item Copy the \code{sampling} example\footnote{Located
  in \code{/usr/local/prelude-1.3/Examples/sampling_loop}.} to your home directory. Compile the
  file with \code{preludec -node sampling
    sampling.plu}.
\end{itemize}

The compiler creates two files in a directory named \code{sampling_c}:
\code{sampling.h} and \code{sampling.c}. The latter  includes a
file \code{sampling_includes.h}, which must be provided by the user
and defines the C prototypes for the imported nodes, the sensors, and
the actuators. Imported nodes have the same name in \prelude and
C. The functions for sensors are prefixed with \code{input_} in C, and
the functions for actuators are prefixed with \code{output_}.

Several options are available to print the results of the static analyses.
Try the following:
\begin{itemize}
\item \code{preludec -node sampling -print_types sampling.plu}.
\item Same with \code{-print_clocks}.
\end{itemize}

\subsection{Simulation with \schedmcore}
The actual implementation of the imported nodes can be found
in the file \code{sampling_includes.h}. 
This file and the file
\code{sampling.c} have to be compiled and linked together in a shared
library. As this part is tedious to perform manually and has little
educational value, it can be left to a build tool like
\code{cmake}.

The following steps are necessary to set up building with
\code{cmake}:
\begin{itemize}
\item Create a build directory and go there: 
  \code{mkdir build; cd build}
\item Run \code{cmake} to create the actual Makefile:\\
  \code{cmake -DPRELUDE_PATH_HINT=<path_to_prelude> \\}\\*
\code{<path_to_prelude_lab>/sampling_loop}
\item Build a library that is used to execute the example:
  \code{make}. In case you are interested in the precise commands
  executed by make, you can alternatively use \code{make VERBOSE=1}.
\end{itemize}

The build created the shared library \code{libsampling-endoced.so}, which
implements the model and contains the necessary information for
\schedmcore.  After these steps, simply execute \code{make} to re-build
the library.

The library can be executed with \code{lsmc_run-nort -l
  ./libsampling-encoded.so}.  The duration of a tick can be controlled with the
option \code{-b <N>}. Set the duration to one milisecond (\code{-b
  1000}) as the default is far too slow for our example. By default,
\schedmcore uses EDF scheduling, but for simulation any scheduler fits.

\begin{exercise}
  Try some simple modifications of the example:
  \begin{itemize}
  \item Change the input period to 300, execute, then retry with a
    period of 200;\ifthenelse{\boolean{reponse}}{
  \texttt{(Deadline overrun with 200)}
}

  \item Fall back to the initial period of 500 and modify your program
    so that it prints 5 times the same value instead of 3 times;
  \item Play with the clock calculus, for instance, see what happens when
    the factors of \code{*^} and \code{/^} are different;
  \item Change the sensor so that it counts from 9 to 0 and again.\ifthenelse{\boolean{reponse}}{
  \texttt{(This is done directly in \code{sampling_includes})}
}

  \end{itemize}
\end{exercise}

\begin{exercise}\label{exo-tracing}
\prelude can automatically add tracing information to the generated
code. Modify the file \code{CMakeLists.txt} in the folder
\code{sampling_loop}.  Add \code{TRACING values} (if you want to see
the values exchanged) or \code{TRACING instances} (if you want to see
additionally which task instance produced a value) after
\code{NOENCODING}. Re-build the example and execute it.
\ifthenelse{\boolean{reponse}}{
  \texttt{(This prints input/output values of each node at each
  instanciation.)}
}

\end{exercise}

\section{Modeling with \prelude}

\begin{exercise}[Simplified flight control]
\ifthenelse{\boolean{reponse}}{
  \texttt{(A solution is available in the svn.)}
}

We consider the simplified Flight Control System of Fig. \ref{fig:CDV}. 
This system controls the attitude, the trajectory and
the speed of an airplane.
It consists of 7 tasks which execute repeatedly
at a periodic rate. The fastest sub-system executes at 10~ms, it acquires
the state of the system (angles, position, acceleration) and computes
the feedback law of the system. The order is then sent to the flight control surfaces.
The intermediate sub-system is the
piloting loop, it executes at 40~ms and determines the angle to apply. 
The slowest sub-system is the navigation loop,
it executes at 120~ms and determines the acceleration to apply. The
required position of the airplane is acquired at the slow rate.

\begin{figure}[hbt]
\centering
\resizebox{\textwidth}{!}{
   \begin{tikzpicture}[node distance=1.5cm, very thick]
     \tikzstyle{block} = [draw,minimum height=2em,
     minimum width=6em, inner sep=5pt];
     \tikzstyle{txt} = [text centered, inner sep=5pt];
   
     \node[block] (Lg) {\begin{tabular}{c}Navigation Law\\($N_L$)\end{tabular}};
     \path (Lg)+(0,-5.5) node[block] (Fg){\begin{tabular}{c}Navigation Filter\\($N_F$)\end{tabular}};
     \path (Lg)+(-3.5,0) node[txt] (cockpit){};
     \path (Lg)+(5,0) node[block] (Lp){\begin{tabular}{c}Piloting Law\\($P_L$)\end{tabular}};
     \path (Lp)+(0,-4.2) node[block] (Fp){\begin{tabular}{c}Piloting Filter\\($P_F$)\end{tabular}};
     \path (Lp)+(0,1) node[txt] (cockpit2){};
     \path (Lp)+(5,0) node[block] (La){\begin{tabular}{c}Feedback\\Law\\($F_L$)\end{tabular}};
     \path (La)+(0,-2.5) node[block] (Fa){\begin{tabular}{c}Feedback\\filter \\($F_F$)\end{tabular}};
     \path (Fa)+(0,-2.5) node[block] (Ap){\begin{tabular}{l}Acceleration\\position\\ acquisition\\($A_P$)\end{tabular}};
     \path (La)+(2.9,0) node[txt] (gouverne){};
     \path (Fa)+(2.9,0) node[txt] (capteur1){};
     \path (Ap.south east)+(2,0.3) node[txt] (capteur2){};    
     \path (Ap.south east)+(-0.2,0.3) node[txt] (initcapteur2){};
     \path (Ap.north east)+(1.8,-0.3) node[txt] (capteur3){};
     \path (Ap.north east)+(-0.2,-0.3) node[txt] (initcapteur3){};
     \path (Ap.north west)+(0.2,-0.2) node[txt] (initfp){};
     \path (Ap.south west)+(0.2,0.5) node[txt] (initfg){};


     \path (Lg)+(-2.5,1.5) node[txt] (period1){};
     \path (period1)+(0,-8) node[txt] (period2){};
     \path (Lp)+(-2,1.5) node[txt] (period3){};
     \path (period3)+(0,-8) node[txt] (period4){};
     \path (La)+(-2,1.5) node[txt] (period5){};
     \path (period5)+(0,-8) node[txt] (period6){};
     \path (La)+(2.7,1.5) node[txt] (period7){};
     \path (period7)+(0,-8) node[txt] (period8){};


     \draw[->] (Fg) -- (Lg) node[pos=0.5,left]{\begin{tabular}{l}Observed \\Position\\(pos\_o)\end{tabular}};
     \draw[->] (cockpit) -- (Lg) node[pos=0.5,above]{\begin{tabular}{l}Required \\Position\end{tabular}}node[pos=0.5,below]{(pos\_c)};

     \draw[->] (Fp) -- (Lp) node[pos=0.5,left]{\begin{tabular}{l}Observed \\Acceleration\\(acc\_o)\end{tabular}};
     \draw[->] (Lg) -- (Lp) node[pos=0.5,above]{\begin{tabular}{l}Required\\Acceleration\end{tabular}}node[pos=0.5,below]{(acc\_c)};

     \draw[->] (Fa) -- (La) node[pos=0.5,left]{\begin{tabular}{l}Observed\\Angle\end{tabular}}node[pos=0.5,right]{(angle\_o)};
     \draw[->] (initfp) -- (Fp) node[pos=0.5,below]{(acc\_i)};
     \draw[->] (initfg) -- (Fg) node[pos=0.5,below]{(pos\_i)};

     \draw[->] (Lp) -- (La) node[pos=0.5,above]{\begin{tabular}{l}Required\\Angle\end{tabular}}node[pos=0.5,below]{(angle\_c)};
     \draw[->] (La) -- (gouverne) node[pos=0.5,above]{order};
     \draw[->] (capteur1) -- (Fa) node[pos=0.5,above]{angle};
     \draw[->] (capteur2) -- (initcapteur2) node[pos=0.5,above]{acceleration}node[pos=0.5,below]{(acc)};
     \draw[->] (capteur3) -- (initcapteur3) node[pos=0.5,above]{position};

     \draw[dashed] (period1) -- (period2);
     \draw[dashed] (period3) -- (period4);
     \draw[dashed] (period5) -- (period6);
     \draw[dashed] (period7) -- (period8);
     \draw[<->,dashed] (period1) -- (period3)node[pos=0.5,above]{120 ms};
     \draw[<->,dashed] (period3) -- (period5)node[pos=0.5,above]{40 ms};
     \draw[<->,dashed] (period5) -- (period7)node[pos=0.5,above]{10 ms};


    
   \end{tikzpicture}}
  \caption{Flight control system}
  \label{fig:CDV}
\end{figure}


In directory \code{fcs}, you find a project that contains a cmake
file and C files to implement the (imported) nodes of the case
study and simulate the sensors/actuators.

\begin{table}[hbt]
  \centering
  \caption{Task parameters}
  \label{tab:params}
  \begin{tabular}{lccccccc}
    \toprule
    Task & $N_L$ & $N_F$ & $P_L$ & $P_F$ & $F_L$ & $F_F$ & $A_P$ \\
    \midrule
    Period & 120 & 120 & 40 & 40 & 10 & 10 & 10 \\
    WCET & 6 & 5 & 4 & 4 & 1 & 1 & 1 \\
    \bottomrule
  \end{tabular}
\end{table}

\begin{itemize}
\item Edit the file \code{fcs.plu} to implement the flight control
  system as shown in Figure~\ref{fig:CDV}. Use the task parameters
  shown in Table~\ref{tab:params}; assume that the WCET of all sensors
  and actuators is 1~ms. Assume moreover that the actuator \code{order}
  has a deadline of 7~ms.
  In this first step, use direct communication
  between the nodes, i.e., do not use \code{fby}.
\item \prelude modifies the deadlines of tasks to
  enforce precedences. Use the option \code{-print_deadlines} to
  display the deadlines for the encoded task set. Is the system
  schedulable ?
\ifthenelse{\boolean{reponse}}{
  \texttt{(No, some deadlines are negative.)}
}

\item The \code{fby} operator can be used to break direct
  dependencies. Use this operator to delay the flows from $N_L$ to
  $P_L$ and from $P_L$ to $F_L$. Use an initial value of 1. Again, use
  \code{-print_deadlines} to examine the deadlines for the task set
  with encoded precedences.
\item Examine the output of the \prelude compiler when using the
  option \code{-print_protocols}. How large are the buffers between
  the different tasks?

\ifthenelse{\boolean{reponse}}{
  \texttt{(Protocols also detail read/write patterns. For read, ``true/false''
    means node reads the next/current buffer cell. For write, ``true/false'' means
    store/do not store value in the buffer (always in the next
    cell)). See course slide 72 for details.}
}

\item Execute the task set with the \schedmcore runner. 
\code{lsmc_run-nort -l ./libfcs.so -b 10000 -c2} (we will see later why we need option -c2).
Note that if you want to reuse the same \code{build} folder, 
you must remove the file
\code{CMakeCache.txt}.
What output is generated by the program?
\ifthenelse{\boolean{reponse}}{
  \texttt{(I shall calculate so that failure is impossible.)}
}

\end{itemize}
\end{exercise}


% \begin{exercise}\label{ex-exo}
% Create a new folder \code{exo}, 
% in which you must copy the files \code{FindPrelude.cmake},
% \code{CMakeParseArguments.cmake}
% and define a \code{CMakeLists.txt},
% \code{exo.plu}, \code{exo.c}, \code{exo_includes.h}.
% The program \code{exo.plu} works as follows:
% there are 2 inputs $i$ and $j$; 3 outputs 
% $v_1$, $v_2$ and $v_3$.
% All actuators and sensors are assumed with a wcet equal to 0.
% There are 3 tasks $\tau_i$, each 
% producing the output $v_i$.
% They have the following attributes
% \begin{center}
%   \begin{tabular}{lccc}
%     \toprule
%     Task & $\tau_1$ & $\tau_2$ & $\tau_3$ \\
%     \midrule
%     Period & 10 & 40 & 10 \\
%     WCET & 5 & 7 & 3 \\
%     \bottomrule
%   \end{tabular}
% \end{center}
% and they exchange
% their data as depicted below.
% \begin{center}
% \resizebox{0.8\linewidth}{!}{
%    \begin{tikzpicture}[scale=0.45]
%    \draw (2,3) rectangle (2.9,4);
%    \draw (2.5,3.5) node {$\tau_1$};
%    \draw[<->] (0,3) -- (5,3);
%    \draw(-0.3,2.7cm) node[scale=0.4] {0};
%    \draw(5,2.7cm) node[scale=0.4] {10};

%    \draw (7.1,3) rectangle (8,4);
%    \draw (7.5,3.5) node {$\tau_1$};
%    \draw[<->] (5.1,3) -- (10.1,3);
%    \draw(10.1,2.7cm) node[scale=0.4] {20};

%    \draw (12.1,3) rectangle (13,4);
%    \draw (12.5,3.5) node {$\tau_1$};
%    \draw[<->] (10.2,3) -- (15.2,3);
%    \draw(15.2,2.7cm) node[scale=0.4] {30};

%    \draw (17.1,3) rectangle (18,4);
%    \draw (17.5,3.5) node {$\tau_1$};
%    \draw[<->] (15.3,3) -- (20.3,3);
%    \draw(20.3,2.7cm) node[scale=0.4] {40};


%    \draw (10,0) rectangle (14,1);
%    \draw (12,0.5) node[scale=0.8] {$\tau_2$};
%    \draw[<->] (7,0) -- (24.3,0);
%   \draw(20.3,-0.3cm) node[scale=0.4] {40};
%   \draw(24.3,-0.3cm) node[scale=0.4] {57};
%   \draw(6.7,-0.3cm) node[scale=0.4] {17};
%   \draw[->] (20.3,0) -- (20.3,1);




%   \draw (2,-3) rectangle (2.9,-2);
%    \draw (2.5,-2.5) node {$\tau_3$};
%    \draw[<->] (0,-3) -- (5,-3);
%    \draw(0,-3.3cm) node[scale=0.4] {0};
%    \draw(5,-3.3cm) node[scale=0.4] {10};

%    \draw (7.1,-3) rectangle (8,-2);
%    \draw (7.5,-2.5) node {$\tau_3$};
%    \draw[<->] (5.1,-3) -- (10.1,-3);
%    \draw(10.1,-3.3cm) node[scale=0.4] {20};

%    \draw (12.1,-3) rectangle (13,-2);
%    \draw (12.5,-2.5) node {$\tau_3$};
%    \draw[<->] (10.2,-3) -- (15.2,-3);
%    \draw(15.2,-3.3cm) node[scale=0.4] {30};

%    \draw (17.1,-3) rectangle (18,-2);
%    \draw (17.5,-2.5) node {$\tau_3$};
%    \draw[<->] (15.3,-3) -- (20.3,-3);
%    \draw(20.3,-3.3cm) node[scale=0.4] {40};



%   \draw[->] (8,3) -- (10,1);
%   \draw(9,2) node[scale=0.8] {$v_1$};

%   \draw[->] (14,0) -- (17.1,-2);
%   \draw(15.5,-1.5) node[scale=0.8] {$v_2$};

%   \draw[->] (7,-0.75) -- (7,0);
%   \draw(7,-1) node[scale=0.8] {$i$};
%  \draw[->] (0,2.25) -- (0,3);
%   \draw(0,2) node[scale=0.8] {$j$};
%  \draw[->] (5.1,2.25) -- (5.1,3);
%   \draw(5.1,2) node[scale=0.8] {$j$};
%  \end{tikzpicture}}
% \end{center}




% \end{exercise}


\section{Schedulability analysis with  converter}

The \schedmcore converter can be used to check the schedulability of a
task set generated by \prelude\ (or encoded in other formats).  The task
set is translated into a C model and the execution of this model tells us
whether the system is schedulable or not.

The \code{lsmc_converter} supports several policies, which can be
specified with the option \code{-p} (including the classic
\textsc{gedf}, \textsc{gllf}, \textsc{llref}, along with some more
specific policies).

\begin{exercise}

  Perform a schedulability analysis of the flight control system
  for EDF scheduling on a uniprocessor with the C model.
  \begin{itemize}
  \item Generate the model: \code{lsmc_converter -c 1 -m c -p GEDF -l ./libfcs.so}
  \item Compile the model: \code{gcc -o model libfcs.so_GEDF.c}
  \item Run the model: \code{./model}. Is the task set schedulable?
  \item If you want some details on the execution, compile with the option
    \code{gcc -o model -DDEBUG libfcs.so_GEDF.c} and run again;
    \ifthenelse{\boolean{reponse}}{
      \texttt{(Only the left column matters. It prints the simulation
        step by step. At each time unit, it prints for each task:
        remaining execution time, remaining time to next deadline,
        remaining time to next release, task instance number. Red means
        currently executing, green executed at least for one time unit,
        white otherwise. System becomes non-schedulable when remaining execution
        time=remaining time to next deadline.)}
    }


  \item Try again, with two cores (\code{-c 2}).
  \end{itemize}
\end{exercise}
% format du debug : exec_restant, distance_di, distance_ti, nb_inst

\end{document}
