# - Find Prelude compiler
# Find the Prelude synchronous language compiler with associated includes path.
# This module defines
#  PRELUDE_COMPILER, the prelude compiler
#  PRELUDE_COMPILER_VERSION, the version of the prelude compiler
#  PRELUDE_INCLUDE_DIR, where to find dword.h, etc.
#  PRELUDE_FOUND, If false, Prelude was not found.
# One can set PRELUDE_PATH_HINT before using find_package(Prelude) and the
# module with use the PATH as a hint to find preludec.
#
# The hint can be given on the command line too:
#   cmake -DPRELUDE_PATH_HINT=/DATA/ERIC/Prelude/prelude-1.2 /path/to/source
#
# The module defines some functions:
#   Prelude_Compile(NODE <Prelude Main Node> PLU_FILES <Prelude files> [NOENCODING] [TRACING fmt])
#

if(PRELUDE_PATH_HINT)
  message(STATUS "FindPrelude: using PATH HINT: ${PRELUDE_PATH_HINT}")
else()
  set(PRELUDE_PATH_HINT)
endif()

#One can add his/her own builtin PATH.
#FILE(TO_CMAKE_PATH "/DATA/ERIC/Prelude/prelude-1.1" MYPATH)
#list(APPEND PRELUDE_PATH_HINT ${MYPATH})

# FIND_PROGRAM twice using NO_DEFAULT_PATH on first shot
find_program(PRELUDE_COMPILER
  NAMES preludec
  PATHS ${PRELUDE_PATH_HINT}
  PATH_SUFFIXES bin
  NO_DEFAULT_PATH 
  DOC "Path to the Prelude compiler command 'preludec'")
  
find_program(PRELUDE_COMPILER 
  NAMES preludec
  PATHS ${PRELUDE_PATH_HINT}
  PATH_SUFFIXES bin
  DOC "Path to the Prelude compiler command 'preludec'")
 
if(PRELUDE_COMPILER)
    # get the path where the prelude compiler was found
    get_filename_component(PRELUDE_PATH ${PRELUDE_COMPILER} PATH)
    # remove bin
    get_filename_component(PRELUDE_PATH ${PRELUDE_PATH} PATH)
    # add path to PRELUDE_PATH_HINT
    list(APPEND PRELUDE_PATH_HINT ${PRELUDE_PATH})
    execute_process(COMMAND ${PRELUDE_COMPILER} -version
        OUTPUT_VARIABLE PRELUDE_COMPILER_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
endif(PRELUDE_COMPILER)

find_path(PRELUDE_INCLUDE_DIR 
          NAMES dword.h
          PATHS ${PRELUDE_PATH_HINT}
          PATH_SUFFIXES lib/prelude
          DOC "The Prelude include headers")

# Check if LTTng is to be supported
if (NOT LTTNG_FOUND)
  option(ENABLE_LTTNG_SUPPORT "Enable LTTng support" OFF)
  if(ENABLE_LTTNG_SUPPORT)
    find_package(LTTng)
    if (LTTNG_FOUND)
      message(STATUS "Will build LTTng support into library...")
      include_directories(${LTTNG_INCLUDE_DIR})
    endif(LTTNG_FOUND)
  endif(ENABLE_LTTNG_SUPPORT)
endif()

# Macros used to compile a prelude library
include(CMakeParseArguments)
function(Prelude_Compile)
  set(options NOENCODING)
  set(oneValueArgs NODE TRACING)
  set(multiValueArgs PLU_FILES USER_C_FILES)
  cmake_parse_arguments(PLU "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
  
  if(PLU_NOENCODING)
    set(PRELUDE_ENCODING "-no_encoding")
  else()
    set(PRELUDE_ENCODING)
  endif()
  
  if (PLU_TRACING)
    set(PRELUDE_TRACING ${PLU_TRACING})
  else()
    set(PRELUDE_TRACING "no")
  endif()    

  add_custom_command(
      OUTPUT ${CMAKE_CURRENT_SOURCE_DIR}/${PLU_NODE}_c/${PLU_NODE}.c 
             ${CMAKE_CURRENT_SOURCE_DIR}/${PLU_NODE}_c/${PLU_NODE}.h
      COMMAND ${PRELUDE_COMPILER} ${PRELUDE_ENCODING} -tracing ${PRELUDE_TRACING} -node ${PLU_NODE} ${PLU_PLU_FILES}
      DEPENDS ${PLU_PLU_FILES}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} 
      COMMENT "Compile prelude source(s): ${PLU_PLU_FILES})"      
  )
  set_source_files_properties(${CMAKE_CURRENT_SOURCE_DIR}/${PLU_NODE}_c/${PLU_NODE}.c
                              ${CMAKE_CURRENT_SOURCE_DIR}/${PLU_NODE}_c/${PLU_NODE}.h 
                              PROPERTIES GENERATED TRUE)
  include_directories(${PRELUDE_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${LTTNG_INCLUDE_DIR})
  add_library(${PLU_NODE} SHARED
              ${PLU_USER_C_FILES}
	          ${PLU_NODE}_c/${PLU_NODE}.c
	          ${PLU_NODE}_c/${PLU_NODE}.h)
  if(LTTNG_FOUND)
    target_link_libraries(${PLU_NODE} ${LTTNG_LIBRARIES})
  endif()
  message(STATUS "Prelude: Added rule for building prelude library: ${PLU_NODE}")
endfunction(Prelude_Compile)

# handle the QUIETLY and REQUIRED arguments and set PRELUDE_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PRELUDE 
                                  REQUIRED_VARS PRELUDE_COMPILER PRELUDE_INCLUDE_DIR)
# VERSION FPHSA options not handled by CMake version < 2.8.2)                                  
#                                  VERSION_VAR PRELUDE_COMPILER_VERSION)
mark_as_advanced(PRELUDE_INCLUDE_DIR)