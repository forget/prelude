#
# CMake script driver used for running native
#
set(NATIVE_COMMENT_STR "#")
set(NATIVE_RUN_OPTIONS "-c;1;-b;1000;-m;6000000;-v;0")
string(REPLACE ";" " " NATIVE_RUN_OPTIONS_STR  "${NATIVE_RUN_OPTIONS}")
set(NATIVE_RUN_COMMAND "${NATIVE_RUNNER} -l ${NATIVE_LIB} ${NATIVE_RUN_OPTIONS_STR}")
file(WRITE  ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR} File generated with the following command\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR} ${NATIVE_RUN_COMMAND}\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR} Column headers are:\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - T: simulation time [s]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - Va: airspeed [m/s]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - az: normal acceleration [m/s^2]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - q: pitch rate [rad/s]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - Vz: vertical speed [m/s]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - h: altitude [m]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - delta_th_c: commanded throttle [-]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR}   - delta_e_c: commanded elevator deflection [rad]\n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR} \n")
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${NATIVE_COMMENT_STR} T,      Va,                 Az,               q,                Vz,               h,                    delta_th_c,        delta_e_c \n")
execute_process(
  COMMAND ${NATIVE_RUNNER} -l ${NATIVE_LIB} ${NATIVE_RUN_OPTIONS}
  RESULT_VARIABLE RUNNER_RES
  OUTPUT_VARIABLE RUNNER_OUT
  ERROR_VARIABLE RUNNER_ERR
  OUTPUT_STRIP_TRAILING_WHITESPACE)
file(APPEND ${NATIVE_OUTPUT_FILE_NAME} "${RUNNER_OUT}\n")
