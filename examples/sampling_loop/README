Executing with Linux
------
Pre-requisites
- Prelude
- ptask (https://github.com/glipari/ptask)
- the liballegro package

To build and execute (the sampling example) in mono-core:
1) Edit the makefile (ptask_wrapper.make) according to your installation
2) make -f ../../share/ptask_wrapper.make NODE=sampling PROG=sampling
3) sudo sh -c "echo -1 > /proc/sys/kernel/sched_rt_runtime_us"
4) ./sampling

For multi-core, compile with ptask_wrapper_aer.make instead.

Executing with SchedMCore
------
Pre-requisites:
- Prelude
- "cmake", a package should be available for most Linux architectures
- SchedMCore (see http://sites.onera.fr/schedmcore/). For the cmake
  step, enable Prelude support (cmake -DENABLE_PRELUDE_SUPPORT=ON).

To build and execute, you can use the following sequence of commands:
1) mkdir build
2) cd build
3) cmake -DPRELUDE_PATH_HINT=/INSTALL_DIR/prelude-x.x/ ..
4) make
5) lsmc_run -l ./libsampling-noencoding.so -s edf -c 2 -b 1000
This will run the program on 2 cores with the EDF scheduling
policy, with a Prelude time unit set to 1ms.

To compile your own program, change the MODEL_NAME variable in file
CMakeLists.txt

To change compilation options, modify the Prelude_Compile variable,
according to the options defined in ../../share/FindPrelude.cmake
