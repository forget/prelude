set(LSMC_HINT_PATH ${CMAKE_SOURCE_DIR}/../../installed/bin)
# Find the SchedMCore converter tool
find_program(LSMC_CONVERTER_EXECUTABLE
             NAMES lsmc_converter
             PATHS ${LSMC_HINT_PATH})
# Find ohter SchedMCore tools at the same location
if (LSMC_CONVERTER_EXECUTABLE)
  get_filename_component(LSMC_HINT_PATH ${LSMC_CONVERTER_EXECUTABLE} DIRECTORY)
endif()

# Find the SchedMCore runner tool
find_program(LSMC_RUNNER_EXECUTABLE
             NAMES lsmc_run-nort lsmc_run
             PATHS ${LSMC_HINT_PATH})
# Find the SchedMCore simulator tool
find_program(LSMC_SIM_EXECUTABLE
             NAMES lsmc_sim
             PATHS ${LSMC_HINT_PATH})
# Retrieve converter version
if (LSMC_CONVERTER_EXECUTABLE)
  execute_process(COMMAND ${LSMC_CONVERTER_EXECUTABLE} --version
    RESULT_VARIABLE CONVERTER_RES
    OUTPUT_VARIABLE CONVERTER_OUT)
  string(REGEX MATCH "[0-9]+(\\.[0-9]+)*(\\.[0-9]+)*" LSMC_CONVERTER_VERSION "${CONVERTER_OUT}")
  message(STATUS "SchedMCore lsmc_converter version is: ${LSMC_CONVERTER_VERSION}")
endif()

if (LSMC_CONVERTER_EXECUTABLE)
  # Deduce install path from CONVERTER name
  get_filename_component(CONVERTER_REAL ${LSMC_CONVERTER_EXECUTABLE} REALPATH)
  # remove filename
  get_filename_component(LSMC_INSTALL_PATH ${CONVERTER_REAL} PATH)
  # remove bin
  get_filename_component(LSMC_INSTALL_PATH ${LSMC_INSTALL_PATH} PATH)
  message(STATUS "SchedMCore discovered install PATH=${LSMC_INSTALL_PATH}")
  # update CMake module path in order to find FindPrelude.cmake
  # coming along with SchedMCore install
  list(APPEND CMAKE_MODULE_PATH "${LSMC_INSTALL_PATH}/share/cmake")
  set(LSMC_INCLUDE_DIRECTORIES "${LSMC_INSTALL_PATH}/include" CACHE PATH "SchedMCore include directories")
else()
  if (NOT LSMC_INCLUDE_DIRECTORIES)
    message(WARNING "Cannot find SchedMCore install PATH because lsmc_converter is not found.")
    message(WARNING "Please define LSMC_INCLUDE_DIRECTORIES cmake variables")
    set(LSMC_INCLUDE_DIRECTORIES "LSMC_INCLUDE_DIRECTORIES-NOTFOUND" CACHE PATH "SchedMCore include directories")
 endif()
endif()