/* ----------------------------------------------------------------------------
 * SchedMCore - A MultiCore Scheduling Framework
 * Copyright (C) 2009-2011, ONERA, Toulouse, FRANCE - LIFL, Lille, FRANCE
 *
 * This file is part of Prelude
 *
 * Prelude is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation ; either version 2 of
 * the License, or (at your option) any later version.
 *
 * Prelude is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY ; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program ; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *---------------------------------------------------------------------------- */

/* This implementation is for illustration purposes only and not meant
   to be used as is. It should be replaced by your own implementation on
   your distributed platform of choice. */

#include <string.h>
#include <stdlib.h>
#include "extern_com.h"

void alloc_buffers() {
  int i,buffer_number;
  struct buffer_desc* buffer_set;

  get_buffer_set(&buffer_number, &buffer_set);
  
  for (i=0; i<buffer_number; i++) {
    buffer_set[i].addr=malloc(buffer_set[i].elem_size*buffer_set[i].buffer_size);
  }
}

void read_val(int id, int idx, int data_size, void *data) {
    int buffer_number;
    struct buffer_desc* buffer_set;
    get_buffer_set(&buffer_number, &buffer_set);
    memcpy(data, buffer_set[id].addr+idx*data_size, data_size);
}

void write_val(int id, int idx, int data_size, const void *data) {
    int buffer_number;
    struct buffer_desc* buffer_set;
    get_buffer_set(&buffer_number, &buffer_set);
    memcpy(buffer_set[id].addr+idx*data_size, data, data_size);
}
